import Player from './Player.js';
import { Push } from 'zeromq';

class SocketServer {
  constructor() {
    this.sender = sender;
    this.playerList = new Map();
    this.connection = this.connection.bind(this);
  }
  connection(client) {

    if (!this.playerList.has(client.username.id)) {
      this.playerList.set(client.username.id, new Player(
        client.username.id,
        client.username.username,
        client.username.avatar,
        client.id
      ));
    }
    else {
      this.playerList.get(client.username.id).socketID = client.id;
    }

    client.on('disconnecting', reason => {
      const diconnectingPlayer = this.playerList.get(client.username.id);

      if (!diconnectingPlayer
        || !diconnectingPlayer.room
        || diconnectingPlayer.room.includes('playersSearchingGame')
      ) return;

      this.handleLeaveGame(diconnectingPlayer);
    });

    client.on('joinRoom', async (room, callback) => {

      const connectedPlayer = this.playerList.get(client.username.id);

      connectedPlayer.room = room;
      connectedPlayer.gameID = (room.includes('-')) ? room.substr(0, room.lastIndexOf('-')) : null;

      //when player left game which already started
      if (connectedPlayer.reconnectionTimer !== null && connectedPlayer.leaveGameTimer === null) {
        switch (room) {
          case connectedPlayer.gameID + '-game':
            this.sendMessage('Core\\Event\\PlayerConnectedToGame', {
              gameID: connectedPlayer.gameID,
              playerID: connectedPlayer.playerID,
              username: connectedPlayer.username,
              avatar: connectedPlayer.avatar
            });
            break;
          case connectedPlayer.gameID + 'playersSearchingGame':
            client.emit('activeGameAvailable', { gameID: connectedPlayer.gameID });
            break;
        }
      }

      client.join(room);
      connectedPlayer.clearTimeouts();
      if (callback) callback();
    });

    client.on('leaveRoom', (room) => {
      const connectedPlayer = this.playerList.get(client.username.id);

      connectedPlayer.room = null;
      connectedPlayer.gameID = null

      client.leave(room);
    });
  }
  sendMessage(type, body) {
    const message = JSON.stringify({
      headers: { type: type },
      body: body
    });
    this.sender.send(message);
  }
  handleLeaveGame(diconnectingPlayer) {
    diconnectingPlayer.leaveGameTimer = setTimeout(async () => {
      
      this.sendMessage('Core\\Event\\PlayerLeftGame', {
        gameID: diconnectingPlayer.gameID,
        playerID: diconnectingPlayer.playerID
      });

      diconnectingPlayer.leaveGameTimer = null;

      diconnectingPlayer.room.includes('-game')
        ? this.startBotAnseringQuestions(diconnectingPlayer.playerID)
        : this.playerList.delete(diconnectingPlayer.playerID);

    }, 3000);

    if (!diconnectingPlayer.room.includes('-game')) {
      return;
    }

    diconnectingPlayer.reconnectionTimer = setTimeout(() => {
      diconnectingPlayer.reconnectionTimer = null;
      this.playerList.delete(diconnectingPlayer.playerID);
    }, 400000);
  }
  async startBotAnseringQuestions(playerID) {
    const player = this.playerList.get(playerID);

    const minSeconds = 10;
    const maxSeconds = 35;
    const diffInMilliseconds = Math.random() * (maxSeconds - minSeconds) * 1000 + minSeconds * 1000;

    player.botTimer = setTimeout(() => {

      if (!this.playerList.get(playerID)?.botTimer) return;

      this.sendMessage('Core\\Event\\BotAnsweredQuestion', {
        gameID: player.gameID,
        playerID: player.playerID,
        isCorrect: Math.random() < 0.5
      });

      this.startBotAnseringQuestions(playerID);

    }, diffInMilliseconds);
  }
  playerFinishedGame(gameID, playerID) {
    const player = this.playerList.get(playerID);

    if (!player) return;

    player.clearTimeouts();
  }
}

const sender = new Push();
await sender.connect(process.env.ZMQ_SOCKET_PUSH_URI);

export default new SocketServer(sender);
