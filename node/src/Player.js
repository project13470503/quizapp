export default class Player {
  constructor(playerID, username, avatar, socketID, room = null, gameID = null, leaveGameTimer = null, reconnectionTimer = null, botTimer = null) {
    this.playerID = playerID;
    this.username = username;
    this.avatar = avatar;
    this.socketID = socketID;
    this.room = room;
    this.gameID = gameID;
    this.leaveGameTimer = leaveGameTimer;
    this.reconnectionTimer = reconnectionTimer;
    this.botTimer = botTimer;
  }
  clearTimeouts() {
    clearTimeout(this.leaveGameTimer);
    clearTimeout(this.reconnectionTimer);
    clearTimeout(this.botTimer);

    this.leaveGameTimer = null;
    this.botTimer = null;
    this.leaveGameTimer = null;
  }
}
