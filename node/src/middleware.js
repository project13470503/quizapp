import jwt from 'jsonwebtoken';
import fs from 'fs';
import * as cookie from 'cookie';

const ERROR_MESSAGES = {
  publicKeyNotFound: `Public key file not found`,
  permissionDenied: (path) => `Permission denied when trying to read the public key file: ${path}`,
  errorReadingKey: (message) => `Error reading public key file: ${message}`,
  tokenRequired: 'Unauthorized: Send token',
  tokenExpired: 'Unauthorized: Token has expired',
  invalidToken: 'Unauthorized: Invalid token',
  tokenNotActive: 'Unauthorized: Token not active yet',
  tokenVerificationFailed: 'Unauthorized: Token verification failed',
};




function verifyJwtToken(token, publicKey) {
  return new Promise((resolve, reject) => {
    jwt.verify(token, publicKey, (err, decoded) => {
      if (err) {
        switch (err.name) {
          case 'TokenExpiredError':
            return reject(new Error(ERROR_MESSAGES.tokenExpired));
          case 'JsonWebTokenError':
            return reject(new Error(ERROR_MESSAGES.invalidToken));
          case 'NotBeforeError':
            return reject(new Error(ERROR_MESSAGES.tokenNotActive));
          default:
            return reject(new Error(ERROR_MESSAGES.tokenVerificationFailed));
        }
      }

      resolve(decoded);
    });
  });
}


export default async function authorizationMiddleware(socket, next) {
    const publicKey = process.env.JWT_PUBLIC_KEY;

    if (!publicKey) {
      console.error(publicKey.message);
      return next(new Error(ERROR_MESSAGES.publicKeyNotFound));
    }

    if(!socket.handshake.headers.cookie) {
      console.error(ERROR_MESSAGES.tokenRequired);
      return next(new Error(ERROR_MESSAGES.tokenRequired));
    }

    const cookies = cookie.parse(socket.handshake.headers.cookie);
    const jwtHp = cookies.jwt_hp;
    const jwtS = cookies.jwt_s;

    if (!jwtHp || !jwtS) {
      console.error(ERROR_MESSAGES.tokenRequired);
      return next(new Error(ERROR_MESSAGES.tokenRequired));
    }

    const token = `${jwtHp}.${jwtS}`;

    try {
      const decodedToken = await verifyJwtToken(token, publicKey);

      socket.username = decodedToken;
      return next();
    } catch (err) {
      console.error(err.message);
      return next(err);
    }
}