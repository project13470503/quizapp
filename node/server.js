import { createServer } from 'https';
import fs from 'fs';
import { Server } from 'socket.io';
import authorization from './src/middleware.js';
import SocketServer from './src/SocketServer.js';
import { Pull } from 'zeromq';

const publicKeyPath = process.env.JWT_PUBLIC_KEY;

if (!publicKeyPath) {
  console.error('JWT_PUBLIC_KEY is not defined.');
  process.exit(1);
}

const sslOptions = {
  key: fs.readFileSync('/etc/letsencrypt/live/quizapp.space.local/privkey.pem'),
  cert: fs.readFileSync('/etc/letsencrypt/live/quizapp.space.local/fullchain.pem'),
};

const httpServer = createServer(sslOptions);

const socketIOServer = new Server(httpServer, {
  cors: {
    origin: process.env.WEBSITE_URL,
    methods: ["GET", "POST"],
    credentials: true
  }
});

socketIOServer.use(authorization);
socketIOServer.on('connection', SocketServer.connection);

httpServer.listen(process.env.SOCKET_PORT,
  () => console.log(`listen on ${process.env.SOCKET_PORT}`)
); 

const receiver = new Pull();
await receiver.bind(process.env.ZMQ_SOCKET_PULL_URI);

for await (const [msg] of receiver) {
  const data = JSON.parse(msg);

  if (data.event === 'playerFinishedGame') {
    SocketServer.playerFinishedGame(data.data.gameID, data.data.playerID);
  }

  if (data.hasOwnProperty('roomsToSend')) {

    const socketRooms = socketIOServer.sockets.adapter.rooms;
    for (const room of data.roomsToSend) {
      if (socketRooms.has(room)) {
        socketIOServer
          .to(room)
          .emit(data.event, JSON.stringify(data.data));

        console.log(data.event + ' fired to ' + room);
      }
    }
  }
}
