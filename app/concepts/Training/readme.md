# Training

The User can select which categories he wants to train for. There must be minimum one category selected but it can be multiple categories too.
A training has minimum three levels. When the training starts the first level contains all the questions. As soon as the user answers a question correctly the question gets pushed into the next level. If the user answers wrong the question gets pushed back to the first level. So you need to answer each question 3 time in a row correctly to finnish the training. 

After the user has finished the training he can select if he wants to train with a 4 level. In the 4 level there are all questions which are two times wrongly answerd. He can repeat this process as much as he wants.
