# Game

A Player can prepare a Game. A Game consits of minimum 20 questions. Each question has points. If you answer correctly your score gets increased by the points the queestion has. The questions are randomly selected based on the players category choice.
The player can select how much players could join the game (between two and four). The player should select how much rounds the game has but it must be a minimum of 10 rounds. 

After starting the game other users could join the game. The host of the game can remove players if he wants to. He also can invite his friends (with a link) to the game. After there are enough players in the game the host can start it.

As soon as the game starts each player sees all questions with there points. He can select a question and answer it. The round from the player gets increased each time he answers a question. If the roundlimit is reached each player sees the result of the game. The player with the most points is the winner.
