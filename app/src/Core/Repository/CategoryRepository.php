<?php
namespace Core\Repository;

interface CategoryRepository {
    public function findAll();
    public function findByIDs(array $categoryIDs);
}