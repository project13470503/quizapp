<?php

namespace Core\Repository;

use Core\Enum\GameStatus;
use Core\Entity\Game;

interface GameReposiotry {
    public function findByID(string $id):?Game;
    public function findPublicGames():array;
    public function findByIDWithPlayers(string $gameID, ?string $questionID = null):?Game;
    public function findByStatus(string $id, string $userID, GameStatus $status):?Game;
    public function findStartedGame(string $gameID, string $userID):?Game;
    public function saveOrUpdate(Game $game):void;
}
