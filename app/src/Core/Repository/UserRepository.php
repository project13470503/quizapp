<?php
namespace Core\Repository;

use Core\Entity\User;

interface UserRepository {
    public function findByID(string $id):?User;
    public function save(User $user):void;
    public function existsByUsername(string $username):bool;
    public function existsByEmail(string $emaul):bool;
}
