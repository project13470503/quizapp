<?php
namespace Core\Repository;

use Iterator;
use Core\Entity\Training;

interface TrainingRepository {
    public function findTrainingsForUser(string $userID);
    public function saveOrUpdate(Training $training):void;
    public function findByID(string $trainingID, ?string $questionID):?Training;
    public function findQuestionsForLevel(string $trainingID, string $levelID):Iterator;
    public function trainingExistsWithCategories(string $userID, array $catefories):bool;
}
