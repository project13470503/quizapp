<?php
namespace Core\Repository;

use Core\Entity\Question;

interface QuestionRepository {
    public function findQuestionsPerCategory(int $questionsPerCategoryLimit, ?int $questionLimit = null):array;
    public function findIDsByCategories(array $categoryIDs):array;
    public function findByIdWithAnswers(string $id):?Question;
    public function findCorrectAnsweresCountForQuestions(array $questions):int;
}
