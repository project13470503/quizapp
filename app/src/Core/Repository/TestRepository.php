<?php
namespace Core\Repository;

use Core\Entity\Test;

interface TestRepository {
    public function findAllByUserID(string $userID);
    public function findQuestionsInTest(string $testID, string $userID):array;
    public function saveOrUpdate(Test $test):void;
    public function delete(Test $test):void;
    public function findByID(string $id, string|null $questionID = null):?Test;
    public function userHasRunningTest(string $userID):bool;
}
