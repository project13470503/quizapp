<?php

namespace Core\Messaging;

interface EventBus
{
    public function dispatch(Event $event):void;
}