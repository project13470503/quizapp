<?php
namespace Core\Messaging;

use Core\Messaging\Event;

class EventRegistry
{
    private static array $events = [];

    public static function raise(Event $event): void
    {
        self::$events[] = $event;
    }

    public static function popEvents(): array
    {
        $events = self::$events;
        self::$events = [];
        return $events;
    }
}
