<?php

namespace Core\Messaging;

use JsonSerializable;

interface Event extends JsonSerializable
{
    public function getEventName():string;
}
