<?php
namespace Core\EventHandler;

use Core\Repository\TrainingRepository;
use Core\Messaging\EventHandler;
use Core\Event\QuestionCorrectAnsweredEvent;

class QuestionCorrectAnsweredEventHandler implements EventHandler {

    public function __construct(
        private TrainingRepository $trainingRepository
    ){}

    public function __invoke(QuestionCorrectAnsweredEvent $event):void
    {
        $training = $this->trainingRepository->findByID($event->trainingID, $event->questionID);

        if(!$training) return;

        if($nextLevelID = $training->getNextLevelID($event->levelID)) {
            $training->unlockQuestion($nextLevelID, $event->questionID);
            $this->trainingRepository->saveOrUpdate($training);
        }
    }
}
