<?php
namespace Core\EventHandler;

use Core\Repository\UserRepository;
use Core\Repository\GameReposiotry;
use Core\Messaging\EventHandler;
use Core\Event\PlayerConnectedToGame;
use Core\Exception\PlayerAlreadyInGameException;
use Core\Exception\PlayerNotInGameException;
use Psr\Log\LoggerInterface;

class PlayerConnectedtoGameEventHandler implements EventHandler {

  public function __construct(
    private GameReposiotry $gameReposiotry,
    private UserRepository $userRepository,
    private LoggerInterface $logger
  ){}

  public function __invoke(PlayerConnectedToGame $event):void
  {
      $game = $this->gameReposiotry->findByIDWithPlayers($event->gameID);

      $user = $this->userRepository->findByID($event->playerID);

      if(!$game) {
        $this->logger->error(sprintf("Spiel mit der ID %s wurde nicht gefunden. beim wiederverbinden zum Spiel", $event->gameID));
      }

      if(!$user) {
        $this->logger->error(sprintf("Spiel mit der ID %s wurde nicht gefunden. beim wiederverbinden zum Spiel", $event->playerID));
      }

      try {
        $game->connect($user);
      } catch(PlayerNotInGameException $e) {
        $this->logger->error($e->getMessage());
      } catch(PlayerAlreadyInGameException) {
        $this->logger->error('Spieler mit der ID %s kann sich nicht wiederverbinden. Er befindet sicht bereits im Spiel');
      }

      $this->gameReposiotry->saveOrUpdate($game);
  }
}
