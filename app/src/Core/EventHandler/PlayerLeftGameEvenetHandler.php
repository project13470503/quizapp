<?php
namespace Core\EventHandler;

use Core\Repository\GameReposiotry;
use Core\Messaging\EventHandler;
use Core\Event\PlayerLeftGame;

class PlayerLeftGameEvenetHandler implements EventHandler {

  public function __construct(private GameReposiotry $gameReposiotry){}

  public function __invoke(PlayerLeftGame $event):void
  {
      $game = $this->gameReposiotry->findByIDWithPlayers($event->gameID);

      if(!$game) return;

      $game->leave($event->playerID);

      $this->gameReposiotry->saveOrUpdate($game);
  }
}
