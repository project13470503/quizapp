<?php
namespace Core\EventHandler;


use Core\Repository\GameReposiotry;
use Core\Messaging\EventHandler;
use Core\Event\BotAnsweredQuestion;

class BotAnsweredQuestionEventHandler implements EventHandler {

  public function __construct(private GameReposiotry $gameReposiotry){}

  public function __invoke(BotAnsweredQuestion $event):void
  {
      $game = $this->gameReposiotry->findByIDWithPlayers($event->gameID);

      if(!$game) {
        return;
      }

      $game->questionAnsweredByBot($event->playerID, $event->isCorrect);

      $this->gameReposiotry->saveOrUpdate($game);
  }
}
