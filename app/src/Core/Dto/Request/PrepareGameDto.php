<?php

namespace Core\Dto\Request;

use Symfony\Component\Validator\Constraints as Assert;
use Core\Enum\GameMode;

class PrepareGameDto {
    #[Assert\Choice(
        choices: [GameMode::SINGLEPLAYER->value, GameMode::MULTIPLAYER->value],
        multiple: false,
        message: 'Der Spielmodus ist falsch. Richtige Optionen sind {{ choices }}',
    )]
    public string $gameMode;
    #[Assert\LessThanOrEqual(4, message: 'Es dürfen nur maximal {{ compared_value }} Spieler in einem Spiel sein')]
    public int $playersAllowed = 2;
}
