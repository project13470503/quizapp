<?php
namespace Core\Dto\Request;

class FindGameWaitingForPlayersDto {

    public function __construct(
        public string $id,
       )
    {
    }
}
