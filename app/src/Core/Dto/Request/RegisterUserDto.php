<?php

namespace Core\Dto\Request;

use Symfony\Component\Validator\Constraints as Assert;

class RegisterUserDto {
   #[Assert\NotBlank(message: 'Der Vorname darf nicht leer sein')]
   #[Assert\Length(min:2, max: 50, minMessage: 'Der Vorname muss mindestens {{ limit }} lang sein', maxMessage: 'Der Vorname darf maximal {{ limit }} lang sein')]
   public string $firstname;
   #[Assert\NotBlank(message: 'Der Nachname darf nicht leer sein')]
   #[Assert\Length(min:2, max: 50, minMessage: 'Der Nachname muss mindestens {{ limit }} lang sein', maxMessage: 'Der Nachname darf maximal {{ limit }} lang sein')]
   public string $lastname;
   #[Assert\NotBlank(message: 'Die Email darf nicht leer sein')]
   #[Assert\Email(mode:'strict', message: 'Die Email muss eine gültige Email sein')]
   public string $email;
   #[Assert\Length(min:2, max: 50, minMessage: 'Der Username muss mindestens {{ limit }} lang sein', maxMessage: 'Der Username darf maximal {{ limit }} lang sein')]
   #[Assert\NotBlank(message: 'Der Username darf nicht leer sein')]
   public string $username;
   #[Assert\Length(min:4, minMessage: 'Das Passwort muss mindestens {{ limit }} lang sein')]
   #[Assert\PasswordStrength(minScore:2, message: 'Das Passwort ist nicht sicher genug')]
   public string $password;
   #[Assert\NotBlank(message: 'Passwort Wiederholen darf nicht leer sein.')]
   #[Assert\EqualTo(propertyPath: 'password')]
   public string $passwordRepeat;
}
