<?php

namespace Core\Dto\Request;

use Symfony\Component\Validator\Constraints as Assert;

class StartTrainingDto {
    #[Assert\Type('array', 'Kategorien müssen ein {{ type }} sein')]
    #[Assert\Count(min: 1, minMessage: 'Bitte wählen Sie mindestens eine Kategorie aus')]
    public array $categoryIDs;
}
