<?php
namespace Core\Dto\Request;

use Symfony\Component\Validator\Constraints as Assert;

class AnswerQuestionInTestDto {
    public string $testID;
    public string $questionID;
    #[Assert\Type('array', 'Antworten müssen ein {{ type }} sein')]
    #[Assert\Count(min: 1, max: 4, minMessage: 'Bitte wählen Sie mindestens eine Antwort aus', maxMessage: 'Es gibt nur maximal {{ max }} Antworten')]
    public array $answers = [];

}
