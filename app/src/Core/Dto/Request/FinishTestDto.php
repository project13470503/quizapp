<?php
namespace Core\Dto\Request;

readonly class FinishTestDto {

    public function __construct(public string $id)
    {
    }
}
