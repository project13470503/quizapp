<?php

namespace Core\Dto\Response;

use Core\Enum\GameStatus;

readonly class GameResponse {
    public function __construct(
        public string $id,
        public int $playersInGame,
        public int $maxPlayersInGame,
        public GameStatus $status
    ) {}
}
