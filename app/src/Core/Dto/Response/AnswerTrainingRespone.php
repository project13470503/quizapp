<?php

namespace Core\Dto\Response;

readonly class AnswerTrainingRespone {
    public function __construct(
        public array $correctAnswerIDs,
        public bool $isCorrect,
        public array $levelProgress
    ){}
}
