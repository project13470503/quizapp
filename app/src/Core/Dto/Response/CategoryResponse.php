<?php

namespace Core\Dto\Response;

class CategoryResponse {
    public function __construct(
        public string $id,
        public string $name,
        public string $slug
    ){}
}
