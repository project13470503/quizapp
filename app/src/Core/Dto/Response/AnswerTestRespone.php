<?php

namespace Core\Dto\Response;

readonly class AnswerTestRespone {
    public function __construct(
        public int $questionsAnsweredCount,
        public bool $isFinished,
        public int $currentResult
    ){}
}
