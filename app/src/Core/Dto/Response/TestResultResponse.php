<?php
namespace Core\Dto\Response;

use DateTimeInterface;

readonly class TestResultResponse {
    public string $startTimne;
    public string $endTime;

    public function __construct(
        public string $id,
        public int $correctAnsweredProcent,
        public int $answeredQuestionsCount,
        public int $questionsCount,
        DateTimeInterface $startTimne,
        DateTimeInterface $endTime
    )
    {
        $this->startTimne = $startTimne->format('Y-m-d H:i:s');
        $this->endTime = $endTime->format('Y-m-d H:i:s');
    }
}
