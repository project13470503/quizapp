<?php

namespace Core\Dto\Response;

use Ramsey\Uuid\UuidInterface;

readonly class StartTrainingResponse {
    public function __construct(
        public UuidInterface $trainingID,
        public UuidInterface $levelID
    ){}
}
