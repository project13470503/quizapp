<?php

namespace Core\Dto\Response;

use Ramsey\Uuid\UuidInterface;
use DateTimeImmutable;

readonly class StartTestResponse {
    public function __construct(
        public UuidInterface $id,
        public DateTimeImmutable $startTime,
        public int $totalQuestionsCount
    ){}
}
