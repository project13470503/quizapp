<?php
namespace Core\Service;

use Symfony\Component\PasswordHasher\PasswordHasherInterface;
use Core\Repository\UserRepository;
use Core\Exception\UserAlreadyExistsException;
use Core\Entity\User;
use Core\Dto\Request\RegisterUserDto;

class UserService {

    public function __construct(
        private PasswordHasherInterface $passwordHasher,
        private UserRepository $userRepository,
        private UserIdentity $userIdentityService
    ){}

    public function signUp(RegisterUserDto $registerUserDto) {

        if($this->userRepository->existsByUsername($registerUserDto->username, $registerUserDto->email)) {
            throw new UserAlreadyExistsException($registerUserDto->username);
        }

        if($this->userRepository->existsByEmail($registerUserDto->email)) {
            throw new UserAlreadyExistsException($registerUserDto->email);
        }

        $user = new User(
            $registerUserDto->firstname,
            $registerUserDto->lastname,
            $registerUserDto->email,
            $registerUserDto->username,
            $this->passwordHasher->hash($registerUserDto->password),
        );

        $this->userRepository->save($user);
    }

    public function addAvatar(string $avatar):string|null {
        $user = $this->userIdentityService->getCurrentUser();

        $currentAvatar = $user?->getAvatar()?->getAvatarName();

        $user->addAvatar($avatar);

        $this->userRepository->save($user);

        return $currentAvatar;
    }
}
