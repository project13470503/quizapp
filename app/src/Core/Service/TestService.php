<?php
namespace Core\Service;

use Core\Repository\TestRepository;
use Core\Repository\QuestionRepository;
use Core\Exception\TestNotFoundExeption;
use Core\Exception\TestNotFinishedException;
use Core\Entity\Test;
use Core\Dto\Response\StartTestResponse;
use Core\Dto\Response\AnswerTestRespone;
use Core\Dto\Request\FinishTestDto;
use Core\Dto\Request\AnswerQuestionInTestDto;

class TestService {

    public function __construct(
        private TestRepository $testRepository,
        private QuestionRepository $questionRepository,
        private UserIdentity $userIdentityService
    ){}

    public function startTest():StartTestResponse
    {
        $user = $this->userIdentityService->getCurrentUser();

        if($this->testRepository->userHasRunningTest($user->getID())) {
            throw new TestNotFinishedException();
        }

        $questions = $this->questionRepository->findQuestionsPerCategory(questionsPerCategoryLimit: 5, questionLimit: 25);

        $correctAnswersCount = $this->questionRepository->findCorrectAnsweresCountForQuestions($questions);

        $test = new Test($user, $correctAnswersCount, $questions);

        $this->testRepository->saveOrUpdate($test);

        return new StartTestResponse(
            $test->getID(),
            $test->getStartTime(),
            $test->getTotalQuestionsCount()
        );
    }

    public function answerQuestionInTests(AnswerQuestionInTestDto $dto):AnswerTestRespone {
        $test = $this->findTest($dto->testID, $dto->questionID);

        $question = $this->questionRepository->findByIdWithAnswers($dto->questionID);

        $correctness = $question->checkAnswers($dto->answers);

        $correctAnswerCount = count(array_diff($correctness, [false]));

        $test->answerQuestion($dto->questionID, $dto->answers, $correctAnswerCount);

        $this->testRepository->saveOrUpdate($test);

        return new AnswerTestRespone(
            $test->getQuestionsAnsweredCount(),
            $test->isFinished(),
            $test->getCorrectAnsweredProcent()
        );
    }

    public function getTestsForUser():array {
        $userID = $this->userIdentityService->getCurrentUser()->getID();
        return $this->testRepository->findAllByUserID($userID);
    }

    public function findQuestionsForTest(string $testID):array {

        $userID = $this->userIdentityService->getCurrentUser()->getID();

        return $this->testRepository->findQuestionsInTest($testID, $userID);
    }


    public function finish(FinishTestDto $dto):array {
        $test = $this->findTest($dto->id);

        if(!$test->isFinished()) {
            $test->finish();
            $this->testRepository->saveOrUpdate($test);
        }

        return [
            'correctAnsweredProcent' => $test->getCorrectAnsweredProcent(),
            'isPositive' => $test->isPositive(),
            'endTime' => $test->getEndTime()
        ];
    }

    public function delete(string $testID)  {
        $test = $this->findTest($testID);

        $this->testRepository->delete($test);
    }

    public function findTest(string $testID, string|null $questionID = null):Test {
        $test = $this->testRepository->findByID($testID, $questionID);

        if($test === null) {
            throw new TestNotFoundExeption($testID);
        }

        return $test;
    }
}
