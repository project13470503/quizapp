<?php

namespace Core\Service;

use Core\Repository\QuestionRepository;
use Core\Repository\GameReposiotry;
use Core\Exception\GameNotFoundException;
use Core\Enum\GameStatus;
use Core\Enum\GameMode;
use Core\Entity\Game;
use Core\Dto\Request\PrepareGameDto;
use Core\Dto\Request\AnswerQuestionInGameDto;

class GameService {

    public function __construct(
        private GameReposiotry $gameRepository,
        private QuestionRepository $questionRepository,
        private UserIdentity $userIdentityService
    ){}


    public function prepareGame(PrepareGameDto $dto):string {
        $user = $this->userIdentityService->getCurrentUser();

        $questions = $this->questionRepository->findQuestionsPerCategory(questionsPerCategoryLimit: 5, questionLimit: 10);

        $game = new Game(
            questions: $questions,
            maxPlayersInGame: $dto->playersAllowed,
            mode: GameMode::from($dto->gameMode),
            host: $user
        );

        $this->gameRepository->saveOrUpdate($game);

        return $game->getID();
    }


    public function start(string $gameID):void {

        $user = $this->userIdentityService->getCurrentUser();

        $game = $this->gameRepository->findByID($gameID);

        if(!$game) {
            throw new GameNotFoundException($gameID);
        }

        $game->start($user->getID());

        $this->gameRepository->saveOrUpdate($game);
    }


    public function join(string $gameID):bool {
        $user = $this->userIdentityService->getCurrentUser();

        $game = $this->gameRepository->findByIDWithPlayers($gameID);

        if(!$game) {
            throw new GameNotFoundException($gameID);
        }

        $game->join($user);

        $this->gameRepository->saveOrUpdate($game);

        return $game->getStatus() === GameStatus::STARTED;
    }


    public function leave(string $gameID) {
        $user = $this->userIdentityService->getCurrentUser();

        $game = $this->gameRepository->findByIDWithPlayers($gameID);

        if(!$game) {
            throw new GameNotFoundException($gameID);
        }

        $game->leave((string)$user->getID());

        $this->gameRepository->saveOrUpdate($game);
    }


    public function checkCorrectness(AnswerQuestionInGameDto $dto):void
    {
        $userID = $this->userIdentityService->getCurrentUser()->getID();

        $game = $this->gameRepository->findByIDWithPlayers($dto->gameID, $dto->questionID);

        if(!$game) {
            throw new GameNotFoundException($dto->gameID);
        }

        $game->allowedToAnswerQuestion($userID, $dto->questionID);

        $question = $this->questionRepository->findByIdWithAnswers($dto->questionID);

        $isCorrectAnswered = !in_array(false, $question->checkAnswers($dto->answers));

        $game->questionAnswered($userID, $dto->questionID, $isCorrectAnswered);

        $this->gameRepository->saveOrUpdate($game);
    }


    public function findGame(string $gameID, GameStatus $status) {
        $currentUserID = $this->userIdentityService->getCurrentUser()->getID();

        $game = $this->gameRepository->findByStatus($gameID, $currentUserID, $status);

        if(!$game) {
            throw new GameNotFoundException($gameID);
        }

        return $game;
    }

    public function getPublicGames() {
        return $this->gameRepository->findPublicGames();
    }

    public function findStartedGame(string $gameID):Game {
        $currentUserID = $this->userIdentityService->getCurrentUser()->getID();

        $game =  $this->gameRepository->findStartedGame($gameID, $currentUserID);

        if(!$game) throw new GameNotFoundException($gameID);

        return $game;
    }
}
