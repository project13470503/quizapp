<?php

namespace Core\Service;

interface QuestionDataLoader {
    public function load(string $fielLocation):array;
    public function loadMultiple(array|string $fielLocations):array;
}