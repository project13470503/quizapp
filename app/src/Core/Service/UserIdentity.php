<?php
namespace Core\Service;

use Core\Entity\User;

interface UserIdentity {
    public function getCurrentUser():User;
}