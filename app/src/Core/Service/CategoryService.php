<?php
namespace Core\Service;

use Core\Repository\CategoryRepository;

class CategoryService {

    public function __construct(private CategoryRepository $categoryRepository){}

    public function findAllCategories() {
        return $this->categoryRepository->findAll();
    }
}
