<?php

namespace Core\Service;

use Iterator;
use Core\Repository\TrainingRepository;
use Core\Repository\QuestionRepository;
use Core\Repository\CategoryRepository;
use Core\Exception\TrainingNotFoundException;
use Core\Exception\TrainingAlreadyExistsException;
use Core\Exception\InvalidCategorySelection;
use Core\Entity\Training;
use Core\Dto\Response\StartTrainingResponse;
use Core\Dto\Response\AnswerTrainingRespone;
use Core\Dto\Request\StartTrainingDto;
use Core\Dto\Request\AnswerQuestionInTrainingDto;

class TrainingService {

    public function __construct(
        private TrainingRepository $trainingRepository,
        private QuestionRepository $questionRepository,
        private CategoryRepository $categoryRepository,
        private UserIdentity $userIdentityService
    ){}


    public function startTraining(StartTrainingDto $dto):StartTrainingResponse {
        $user = $this->userIdentityService->getCurrentUser();

        $categories = $this->categoryRepository->findByIDs($dto->categoryIDs);

        if(count($categories) !== count($dto->categoryIDs)) {
            throw new InvalidCategorySelection();
        }

        if($this->trainingRepository->trainingExistsWithCategories($user->getID(), $categories)) {
            throw new TrainingAlreadyExistsException();
        }

        $questions = $this->questionRepository->findIDsByCategories($dto->categoryIDs);

        $training = new Training($user, $categories, $questions);

        $this->trainingRepository->saveOrUpdate($training);

        return new StartTrainingResponse(
            $training->getID(),
            $training->getFirstLevelID()
        );
    }


    public function checkCorrectness(AnswerQuestionInTrainingDto $dto):AnswerTrainingRespone {
        $training = $this->trainingRepository->findByID($dto->trainingID, $dto->questionID);

        if(!$training) {
            throw new TrainingNotFoundException($dto->trainingID);
        }

        $question = $this->questionRepository->findByIdWithAnswers($dto->questionID);

        $isCorrect = !in_array(false, $question->checkAnswers($dto->answers));

        $isCorrect
            ? $training->questionCorrectAnswerd($dto->questionID, $dto->levelID)
            : $training->questionWrongAnswerd($dto->questionID, $dto->levelID);

        $this->trainingRepository->saveOrUpdate($training);

        return new AnswerTrainingRespone(
            $question->getCorrectAnswerIDs()->toArray(),
            $isCorrect,
            $training->getLevels()->toArray()
        );
    }


    public function getTrainingsForUser():array {
        $userID = $this->userIdentityService->getCurrentUser()->getID();

        return $this->trainingRepository->findTrainingsForUser($userID);
    }


    public function getQuestions(string $trainingID, string $levelID):Iterator {
        $userID = $this->userIdentityService->getCurrentUser()->getID();

        return  $this->trainingRepository->findQuestionsForLevel($trainingID, $levelID, $userID);
    }
}
