<?php
namespace Core\Enum;

enum GameStatus: string {
    case STARTED = 'started';
    case FINISHED = 'finished';
    case SEARCHING = 'searching';

    public function hasStarted(): bool {
        return match($this) 
        {
            GameStatus::STARTED => true,   
            GameStatus::SEARCHING => false,   
            GameStatus::FINISHED => true   
        };
    }
}