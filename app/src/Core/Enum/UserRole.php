<?php
namespace Core\Enum;

enum UserRole :string {
    case USER = 'USER';
    case ADMIN = 'ADMIN';
}