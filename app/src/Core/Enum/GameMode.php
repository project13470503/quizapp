<?php
namespace Core\Enum;

use InvalidArgumentException;

enum GameMode :string {
    case MULTIPLAYER = 'multiplayer';
    case SINGLEPLAYER = 'singleplayer';

    public static function createFrom(string $value):static {
        $enum = static::tryFrom($value);

        if(!$enum) {
            throw new InvalidArgumentException();
        }

        return $enum;
    }

    public function isMultiplayer(): bool
    {
        return match($this) 
        {
            GameMode::MULTIPLAYER => true,   
            GameMode::SINGLEPLAYER => false
        };
    }

}