<?php

namespace Core\Exception;

use Exception;

#[ErrorCode(409)]
class PlayerAlreadyInGameException extends Exception {
    public function __construct(string $userID)
    {
        parent::__construct('Du befindest dich bereits im Spiel');
    }
}
