<?php
namespace Core\Exception;

#[\Attribute]
class ErrorCode {
    public function __construct(public int $code) {}
}
