<?php

namespace Core\Exception;

#[ErrorCode(422)]
class InvalidCategorySelection extends \Exception {
    public function __construct()
    {
        parent::__construct(sprintf('Eine oder mehrere der ausgwählten Kategorien sind ungültig'));
    }
}
