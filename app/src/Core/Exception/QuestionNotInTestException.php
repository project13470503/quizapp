<?php

namespace Core\Exception;

use Exception;

#[ErrorCode(404)]
class QuestionNotInTestException extends Exception {

    public function __construct(string $questionID, string $testID)
    {
        parent::__construct(sprintf('Question mit der ID: %s befindet sich nicht im Test mit der ID: %s', $questionID, $testID));
    }
}
