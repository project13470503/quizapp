<?php

namespace Core\Exception;

use Exception;

#[ErrorCode(409)]
class GameAlreadyStartedException extends Exception {
    public function __construct(string $id)
    {
        parent::__construct(sprintf('Spieler mit der ID: %s hat bereits gestartet', $id));
    }
}
