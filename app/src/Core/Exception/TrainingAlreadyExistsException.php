<?php

namespace Core\Exception;

use Exception;

#[ErrorCode(409)]
class TrainingAlreadyExistsException extends Exception {
    public function __construct()
    {
        parent::__construct('Es exestiert bereits ein Training mit den ausgewälten Kategorien');
    }
}
