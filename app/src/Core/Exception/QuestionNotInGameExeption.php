<?php
namespace Core\Exception;

use Exception;

#[ErrorCode(404)]
class QuestionNotInGameExeption extends Exception{
    public function __construct(string $questionID)
    {
        parent::__construct(sprintf('Frage mit der ID: %s befindet sich nicht im Spiel', $questionID));
    }
}
