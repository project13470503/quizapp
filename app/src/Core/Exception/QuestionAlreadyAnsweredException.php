<?php

namespace Core\Exception;

use Exception;

#[ErrorCode(409)]
class QuestionAlreadyAnsweredException extends Exception {
    public function __construct(string $id)
    {
        parent::__construct(sprintf('Frage mit der ID: %s wurde bereits beantwortet', $id));
    }
}
