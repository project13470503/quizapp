<?php

namespace Core\Exception;

use Exception;

#[ErrorCode(404)]
class QuestionNotFoundException extends Exception {

    public function __construct(string $id)
    {
        parent::__construct( sprintf('Frage mit der ID: %s konnte nicht gefunden werden', $id));
    }
}
