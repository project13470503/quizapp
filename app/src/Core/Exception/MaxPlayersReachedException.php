<?php

namespace Core\Exception;

use Exception;

#[ErrorCode(409)]
class MaxPlayersReachedException extends Exception {
    public function __construct(string $gameID, int $maxPlayers)
    {
        parent::__construct(sprintf('Das Spiel hat bereits die maximale Spieler Anzahl von %s Spielern erreicht', $maxPlayers));
    }
}
