<?php

namespace Core\Exception;

#[ErrorCode(404)]
class GameNotFoundException extends \Exception {
    public function __construct(string $id)
    {
        parent::__construct(sprintf('Spiel mit der ID: %s konnte nicht gefunden werden', $id));
    }
}
