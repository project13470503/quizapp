<?php
namespace Core\Exception;

use Exception;

#[ErrorCode(404)]
class QuestionNotInLevelExeption extends Exception {
    public function __construct(string $questionID, string $levelID)
    {
        parent::__construct(sprintf('Frage mit der ID: %s befindet sich nicht im Level mit der ID: %s', $questionID, $levelID));
    }
}
