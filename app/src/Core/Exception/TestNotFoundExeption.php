<?php

namespace Core\Exception;

use Exception;

#[ErrorCode(404)]
class TestNotFoundExeption extends Exception {

    public function __construct(string $id)
    {
        parent::__construct(sprintf('Test mit der ID: %s konnte nicht gefunden werden', $id));
    }
}
