<?php

namespace Core\Exception;

#[ErrorCode(404)]
class LevelNotInTrainingException extends \Exception {
    public function __construct(string $levelID, string $trainingID)
    {
        parent::__construct(sprintf('Level mit der ID: %s befindet sich nicht im Training mit der ID: %s', $levelID, $trainingID));
    }
}
