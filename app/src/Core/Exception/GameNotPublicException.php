<?php

namespace Core\Exception;

use Exception;

#[ErrorCode(409)]
class GameNotPublicException extends Exception {
    public function __construct(string $id)
    {
        parent::__construct(sprintf('Spieler mit der ID: %s ist ein privates Spiel', $id));
    }
}
