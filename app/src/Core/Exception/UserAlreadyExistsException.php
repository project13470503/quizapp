<?php

namespace Core\Exception;

#[ErrorCode(422)]
class UserAlreadyExistsException extends \Exception {


    private string $type = '';

    public function __construct(string $property)
    {
        if(filter_var($property, FILTER_VALIDATE_EMAIL)) {
            $this->type = 'email';
            $message = sprintf('Die verwendete E-Mail ist bereits vergeben');
        }
        else {
            $this->type = 'username';
            $message = sprintf('Der verwendete Username ist bereits vergeben');
        }

        parent::__construct($message);
    }

    public function getType():string {
        return $this->type;
    }
}
