<?php

namespace Core\Exception;

use Exception;

#[ErrorCode(404)]
class PlayerNotInGameException extends Exception {
    public function __construct(string $userID)
    {
        parent::__construct(sprintf('Spieler mit der ID: %s befinden sich nicht im Spiel', $userID));
    }
}
