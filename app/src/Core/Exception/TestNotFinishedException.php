<?php

namespace Core\Exception;

use Exception;

#[ErrorCode(409)]
class TestNotFinishedException extends Exception {

    public function __construct()
    {
        parent::__construct('Schließe zuerst deinen aktuellen Test ab bevor du einen neuen startest');
    }
}
