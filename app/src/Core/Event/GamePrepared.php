<?php

namespace Core\Event;

use Core\Messaging\Event;

class GamePrepared implements Event {

    use EventTrait;

    private array $rooms;

    public function __construct(
        public readonly string $gameID,
        public readonly int $maxPlayersInGame
    ){
        $this->rooms = ['playersSearchingGame'];
    }

    public function jsonSerialize(): mixed
    {
        return [
            'event' => $this->getEventName(),
            'roomsToSend' => $this->rooms,
            'data' => [
                'id' => $this->gameID,
                'maxPlayersInGame' => $this->maxPlayersInGame
            ]
        ];
    }
}
