<?php

namespace Core\Event;

use DateTimeInterface;
use Core\Messaging\Event;

class PlayerAnsweredQuestion implements Event {

    use EventTrait;

    private array $rooms;

    public function __construct(
        public readonly string $gameID,
        public readonly string $playerID,
        public readonly int $score,
        private DateTimeInterface $answeredAt
    ){
        $this->rooms = ["$gameID-game"];
    }

    public function jsonSerialize(): mixed
    {
        return [
            'event' => $this->getEventName(),
            'roomsToSend' => $this->rooms,
            'data' => [
                'gameID' => $this->gameID,
                'playerID' => $this->playerID,
                'score' => (string)$this->score,
                'answeredAt' => $this->answeredAt->format('Y-m-d H:i:s')
            ]
        ];
    }
}
