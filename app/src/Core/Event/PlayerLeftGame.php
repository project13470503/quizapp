<?php

namespace Core\Event;

use Core\Messaging\Event;

class PlayerLeftGame implements Event {

    use EventTrait;

    private array $rooms;

    public function __construct(
        public readonly string $gameID,
        public readonly string $playerID
    ){
        $this->rooms = ['playersSearchingGame', "$gameID-playersWaitingInGame", "$gameID-game"];
    }

    public function jsonSerialize(): mixed
    {
        return [
            'event' => $this->getEventName(),
            'roomsToSend' => $this->rooms,
            'data' => [
                'gameID' => $this->gameID,
                'playerID' => $this->playerID
            ]
        ];
    }
}
