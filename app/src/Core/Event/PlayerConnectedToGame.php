<?php

namespace Core\Event;

use Core\Messaging\Event;

class PlayerConnectedToGame implements Event {

    use EventTrait;

    private array $rooms;

    public function __construct(
        public readonly string $gameID,
        public readonly string $playerID,
        public readonly string $username,
        public readonly ?string $avatar
    ){
        $this->rooms = ["$gameID-game"];
    }

    public function jsonSerialize(): mixed
    {
        return [
            'event' => $this->getEventName(),
            'roomsToSend' => $this->rooms,
            'data' => [
                'gameID' => $this->gameID,
                'playerID' => $this->playerID,
                'username' => $this->username,
                'avatar' => $this->avatar
            ]
        ];
    }
}
