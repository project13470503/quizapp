<?php

namespace Core\Event;

use Core\Messaging\Event;

class GameStarted implements Event {

    use EventTrait;

    private array $rooms;

    public function __construct(
        public readonly string $gameID
    ){
        $this->rooms = ['playersSearchingGame', "$gameID-playersWaitingInGame"];
    }

    public function jsonSerialize(): mixed
    {
        return [
            'event' => $this->getEventName(),
            'roomsToSend' => $this->rooms,
            'data' => [
                'id' => $this->gameID
            ]
        ];
    }
}
