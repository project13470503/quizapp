<?php

namespace Core\Event;

use Core\Messaging\Event;

readonly class QuestionCorrectAnsweredEvent implements Event {

    use EventTrait;

    public function __construct(
        public string $questionID,
        public string $trainingID,
        public string $levelID
    ){}

    public function jsonSerialize(): mixed {
        return [
            'questionID' => $this->questionID,
            'trainingID' => $this->trainingID,
            'levelID' => $this->levelID
        ];
    }
}
