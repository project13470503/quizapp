<?php

namespace Core\Event;

use Core\Messaging\Event;

class BotAnsweredQuestion implements Event {

    use EventTrait;

    public function __construct(
        public readonly string $gameID,
        public readonly string $playerID,
        public readonly bool $isCorrect
    ){}

    public function jsonSerialize(): mixed
    {
        return [
            'event' => $this->getEventName(),
            'data' => [
                'gameID' => $this->gameID,
                'playerID' => $this->playerID,
                'isCorrect' => $$isCorrect
            ]
        ];
    }
}
