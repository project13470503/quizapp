<?php
namespace Core\Event;

trait EventTrait {
    public function getEventName(): string {
        return lcfirst(str_replace(__NAMESPACE__ . '\\', '', __CLASS__));
    }
}
