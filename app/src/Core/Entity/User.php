<?php
namespace Core\Entity;

use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Ramsey\Uuid\Uuid;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;
use Core\Enum\UserRole;

class User implements UserInterface, PasswordAuthenticatedUserInterface{

    use IdTrait;
    private array $roles;
    private ?Avatar $avatar = null;
    private Collection $playedGameStates;

    public function __construct(
        private string $firstname,
        private string $lastname,
        private string $email,
        private string $username,
        private string $password,
    )
    {
        $this->id = Uuid::uuid7();
        $this->roles = [UserRole::USER->value];
        $this->playedGameStates = new ArrayCollection();
    }

    public function getAvatar():?Avatar {
        return $this?->avatar;
    }

    public function hasAvatar():?bool {
        return $this->avatar?->getAvatarName() !== null;
    }

    public function addAvatar($imgUrl) {
        $this->avatar = new Avatar($imgUrl);
    }

    public function eraseCredentials(): void
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    public function getUserIdentifier(): string
    {
        return $this->email;
    }

    public function getFirstname():string {
        return $this->firstname;
    }

    public function getPassword(): string
    {
        return $this->password;
    }

    public function getRoles(): array
    {
        return $this->roles;
    }

    public function getLastname():string {
        return $this->lastname;
    }


    public function getUsername():string {
        return $this->username;
    }

    public function getEmail():string {
        return $this->email;
    }
}
