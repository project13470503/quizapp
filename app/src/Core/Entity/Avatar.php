<?php

namespace Core\Entity;

class Avatar {
    public const AVATAR_PATH = 'avatars/';
    private ?string $avatarName = null;

    public function __construct(string $avatarName)
    {
        $this->avatarName = self::AVATAR_PATH . $avatarName;
    }

    public function getAvatarName():?string {
        return ! (!$this->avatarName || empty($this->avatarName)) ? $this->avatarName : null;
    }
}
