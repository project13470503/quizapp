<?php
namespace Core\Entity;

use Ramsey\Uuid\UuidInterface;
use Ramsey\Uuid\Uuid;
use JsonSerializable;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;

class Question implements JsonSerializable {

    use IdTrait;

    private string $question;
    private Collection $answers;
    private Category|null $category = null;
    private Collection $trainingQuestions;
    private Collection $testQuestions;

    public function __construct(string $question, Category $category)
    {
        $this->id = Uuid::uuid7();
        $this->question = $question;
        $this->category = $category;
        $this->answers = new ArrayCollection();
    }

    public function getCorrectAnswerIDs():Collection {
        return $this->answers
            ->filter(function(Answer $answer) {
                if($answer->isCorrect()) {
                    return $answer;
                }
            })
            ->map(function(Answer $answer) {
                return (string)$answer->getID();
            });
    }

    public function checkAnswers(array $userAnswers):array {
        $correctAnswers = $this->getCorrectAnswerIDs();
        $correctness = [];

        foreach($userAnswers as $userAnswerID) {
            $correctness[] = $correctAnswers->contains($userAnswerID);
        }

        if(count($userAnswers) !== $correctAnswers->count()) {
            $correctness[] = false;
        }

        return $correctness;
    }

    public function getQuestion():string {
        return $this->question;
    }

    public function getAnswers():Collection {
        return $this->answers;
    }

    public function addAnswer(Answer $answer):void {
        $this->answers->add($answer);
    }

    public function getCategory():Category {
        return $this->category;
    }

    public function jsonSerialize():array {
        return [
            'id' => $this->id->toString(),
            'question' => $this->question,
            'answers' => $this->getAnswers()->toArray()
        ];
    }
}
