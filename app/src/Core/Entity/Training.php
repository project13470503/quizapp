<?php
namespace Core\Entity;

use Ramsey\Uuid\UuidInterface;
use Ramsey\Uuid\Uuid;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;
use Core\Messaging\EventRegistry;
use Core\Exception\QuestionNotInLevelExeption;
use Core\Exception\LevelNotInTrainingException;
use Core\Event\QuestionCorrectAnsweredEvent;

class Training {

    use IdTrait;

    private Collection $levels;
    private Collection $categories;
    private User|null $user = null;

    public function __construct(User $user, array $categories, array $questions) {
        $this->id = Uuid::uuid7();
        $this->categories = new ArrayCollection($categories);
        $this->levels = new ArrayCollection();
        $this->user = $user;

        $questionsCount = count($questions);

        for($i = 1; $i <= 3; $i++) {
            $questionsForLevel = ($i === 1) ? $questions : [];
            $this->levels->add(new Level("Level $i", $questionsCount, $questionsForLevel, $this));
        }
    }

    public function getFirstLevelID():UuidInterface {
        return $this->levels->first()->getID();
    }

    public function getCategories():Collection {
        return $this->categories;
    }

    private function getNextLevel(Level $level):Level|null {
        $levelKey = $this->levels->indexOf($level);

        $nextLevel = $this->levels->get($levelKey + 1);

        return (!$nextLevel) ? null : $nextLevel;
    }

    public function getNextLevelID($levelIDBefore):?UuidInterface {
        $level = $this->getLevel($levelIDBefore);

        return $this->getNextLevel($level)?->getID();
    }

    public function getLevels():Collection {
        return $this->levels;
    }

    public function questionCorrectAnswerd(string $questionID, string $levelID):void {
        $level = $this->getLevel($levelID);
        $nextLevel = $this->getNextLevel($level);

        $traingQuestion = $level->getQuestion($questionID);

        if($traingQuestion->isLocked()) {
            throw new QuestionNotInLevelExeption($questionID, $levelID);
        }

        $traingQuestion->lock();

        $level->increaseQuestionsAnsweredCount();
        $level->decreaseQuestionsAvailableCount();

        if($nextLevel !== null){
            $level->removeTrainingQuestion($traingQuestion);
            $nextLevel->addTrainingQuestion($traingQuestion);
            $nextLevel->increaseQuestionsLockedCount();
        }

        EventRegistry::raise(new QuestionCorrectAnsweredEvent($questionID, (string)$this->id, $levelID));
    }

    public function questionWrongAnswerd(string $questionID, $levelID):void
    {
        $level = $this->getLevel($levelID);

        $firstLevel = $this->levels->first();
        $traingQuestion = $level->getQuestion($questionID);

        if($firstLevel->getID()->equals($level->getID())) {
            $traingQuestion->increaseWrongAnsweredCount();
            return;
        }

        $level->removeTrainingQuestion($traingQuestion);
        $level->decreaseQuestionsAvailableCount();

        $firstLevel->addTrainingQuestion($traingQuestion);
        $firstLevel->increaseQuestionsAvailableCount();

        $currentLevelIndex = $this->levels->indexOf($level);

        foreach($this->levels as $index => $level) {
            if($index < $currentLevelIndex) {
                $level->decreaseQuestionsAnsweredCount();
            }
        }
    }

    public function unlockQuestion(string $levelID, string $questionID):void
    {
        $level = $this->getLevel($levelID);

        $level->getQuestion($questionID)->unlock();


        $level->decreaseQuestionsLockedCount();
        $level->increaseQuestionsAvailableCount();
    }

    private function getLevel(string $levelID):Level
    {
        $level = $this->levels->findFirst(fn(int $key, Level $level)
            => (string)$level->getID() === $levelID);

        if(!$level) throw new LevelNotInTrainingException($levelID, $this->id);

        return $level;
    }
}
