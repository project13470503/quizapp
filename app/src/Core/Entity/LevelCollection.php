<?php
namespace Core\Entity;

use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;
use Core\Entity\Level;

class LevelCollection extends ArrayCollection
{
    public function __construct( $elements = [])
    {
        parent::__construct($elements);
    }

    public function getNextLevel(Level $level):?Level {
      return $this->get($this->indexOf($level) + 1);
    }

    public function getCurrentLevel(string $levelID):Level {
        return $this->findFirst(fn(int $key, Level $level)
          => (string)$level->getID() === $levelID);
    }
}
