<?php

namespace Core\Entity;


use Ramsey\Uuid\UuidInterface;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\Rfc4122\UuidV7;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;
use DateTimeInterface;
use DateTimeImmutable;
use Core\Messaging\EventRegistry;
use Core\Exception\QuestionNotInGameExeption;
use Core\Exception\PlayerNotInGameException;
use Core\Exception\PlayerAlreadyInGameException;
use Core\Exception\NotAllowedToStartGameException;
use Core\Exception\MaxPlayersReachedException;
use Core\Exception\GameNotPublicException;
use Core\Exception\GameAlreadyStartedException;
use Core\Event\PlayerLeftGame;
use Core\Event\PlayerJoinedGame;
use Core\Event\PlayerFinishedGame;
use Core\Event\PlayerConnectedToGame;
use Core\Event\PlayerAnsweredQuestion;
use Core\Event\GameStarted;
use Core\Event\GamePrepared;
use Core\Enum\GameStatus;
use Core\Enum\GameMode;

class Game {
    use IdTrait;

    private GameStatus $status;
    protected ?DateTimeInterface $createdAt;
    private Collection $playerGameStates;
    private int $playersInGame = 0;
    private Collection $questions;
    private UuidInterface $hostID;

    public function __construct(private int $maxPlayersInGame, private GameMode $mode, User $host, array $questions)
    {
        $this->id = Uuid::uuid7();
        $this->hostID = $host->getID();
        $this->status = GameStatus::SEARCHING;
        $this->createdAt = new DateTimeImmutable();
        $this->playersInGame++;
        $this->playerGameStates = new ArrayCollection([new PlayerGameState($this, $host)]);
        $this->questions = new ArrayCollection($questions);

        EventRegistry::raise(new GamePrepared(
            $this->getID()->toString(),
            $this->maxPlayersInGame
        ));
    }

    public function getStatus():GameStatus {
        return $this->status;
    }

    public function getMode():GameMode {
        return $this->mode;
    }

    public function getMaxPlayersInGame():int {
        return $this->maxPlayersInGame;
    }

    public function getPlayersInGame():int {
        return $this->playersInGame;
    }

    public function getHostID():UuidInterface {
        return $this->hostID;
    }

    public function getCreatedAt():DateTimeInterface {
        return $this->createdAt;
    }

    public function getPlayerGameStates():Collection {
        return $this->playerGameStates;
    }

    public function getQuestions():Collection {
        return $this->questions;
    }

    private function getPlayer(string $userID):?PlayerGameState {
        return $this->playerGameStates->findFirst(
            fn(int $key, PlayerGameState $player) =>
                $player->getUserID()->equals(UuidV7::fromString($userID)
            )
        );
    }

    public function join(User $player):void {

        if($this->getPlayer((string)$player->getID()) !== null) {
            throw new PlayerAlreadyInGameException($player->getID()->toString());
        }

        if(!$this->mode->isMultiplayer()) {
            throw new GameNotPublicException($this->getID()->toString());
        }

        if($this->status->hasStarted()) {
            throw new GameAlreadyStartedException($this->getID()->toString());
        }

        if($this->maxPlayersInGame === $this->playersInGame) {
            throw new MaxPlayersReachedException($this->id->toString(), $this->maxPlayersInGame);
        }

        $this->playerGameStates->add(new PlayerGameState($this, $player));
        $this->playersInGame++;

        EventRegistry::raise(new PlayerJoinedGame(
            $this->getID()->toString(),
            (string)$player->getID(),
            $player->getUsername(),
            $player->getAvatar()->getAvatarName()
        ));

        if($this->playersInGame === $this->maxPlayersInGame) {
            $this->start($this->hostID);
        }
    }

    public function leave(string $userID):void {

        $player = $this->getPlayer($userID);

        if(!$player) throw new PlayerNotInGameException($userID);

        if($this->status === GameStatus::SEARCHING) {
            $this->playersInGame--;
            $this->playerGameStates->removeElement($player);
            EventRegistry::raise(new PlayerLeftGame($this->getID(), $userID));
        }

        if($this->status === GameStatus::STARTED) {
            $player->disconnect();
            EventRegistry::raise(new PlayerLeftGame($this->getID(), $userID));
        }
    }

    public function connect(User $user):void {

        $curentPlayer = $this->getPlayer($user->getID());

        if(!$curentPlayer) throw new PlayerNotInGameException($user->getID());

        if($curentPlayer->getUser() !== null) {
            throw new PlayerAlreadyInGameException($user->getID());
        }

        $curentPlayer->connect($user);

        EventRegistry::raise(new PlayerConnectedToGame(
            $this->getID(),
            $user->getID(),
            $user->getUsername(),
            $user?->getAvatar()?->getAvatarName()
        ));
    }

    public function start(string $userID):void {

        if($this->hostID->toString() !== $userID) {
            throw new NotAllowedToStartGameException('Nur der Host des Spieles kann das Spiel starten');
        }

        if($this->playersInGame < 2) {
            throw new NotAllowedToStartGameException(sprintf('Es müssen minimum %s spieler im Spiel sein um es zu starten. Aktuell sind %s spieler im Spiel', 2, $this->playersInGame));
        }

        $this->createdAt = new DateTimeImmutable();

        $this->status = GameStatus::STARTED;

        EventRegistry::raise(new GameStarted($this->getID()->toString()));
    }


    private function hasQuestion(string $questionID):bool {
        $foundQuestion = $this->questions->findFirst(fn(int $key, Question $question) => (string)$question->getID() === $questionID);

        return (!$foundQuestion) ? false : true;
    }

    /**
     * Checks if user is allowed to answer a question in Game
     *
     * @throws PlayerNotInGameException
     * @throws QuestionNotInGameExeption
     * @param string $userID
     * @param string $questionID
     * @return void
     */
    public function allowedToAnswerQuestion(string $userID, string $questionID):void {
        $player = $this->getPlayer($userID);

        if(!$player)
            throw new PlayerNotInGameException($this->id, $userID);

        if(!$this->hasQuestion($questionID) || $player->hasQuestionAnswered($questionID))
            throw new QuestionNotInGameExeption($questionID);
    }

    public function questionAnswered(string $userID, string $questionID, bool $correctness):void {
        $player = $this->getPlayer($userID);

        $player->questionWasAnsered($questionID, $correctness);

        EventRegistry::raise(new PlayerAnsweredQuestion(
            $this->getID(),
            $userID,
            $player->getScore(),
            $player->getAnswerdAt()
        ));

        if(!$player->hasFinishedGame()) return;

        if($this->getPlayerFinisedGameCount() === $this->playerGameStates->count()) {
            $this->status = GameStatus::FINISHED;
        }

        EventRegistry::raise(new PlayerFinishedGame(
            $this->getID(), 
            $userID, 
            $player->getUser()->getUsername(), 
            $player->getScore())
        );
    }

    public function questionAnsweredByBot(string $userID, bool $correctness) {
        $curentPlayer = $this->getPlayer($userID);

        if(!$curentPlayer) {
            throw new PlayerNotInGameException($userID);
        }

        $playerAnsweredQuestionIDs = $curentPlayer->getAnsweredQuestionIDs();

        $questionID = $this->questions->findFirst(
            fn(int $key, Question $question) =>
                !in_array((string)$question->getID(), $playerAnsweredQuestionIDs)
            )?->getID();

        $this->questionAnswered($userID, $questionID, $correctness);
    }

    private function getPlayerFinisedGameCount():int {
        return  $this->playerGameStates->filter(
            fn(PlayerGameState $player) => $player->hasFinishedGame()
        )->count();
    }
}
