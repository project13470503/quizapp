<?php
namespace Core\Entity;

use Ramsey\Uuid\Uuid;

class Category {

    use IdTrait;

    private string $name;
    private string $slug;

    public function __construct(string $name, string $slug)
    {
        $this->id = Uuid::uuid7();
        $this->name = $name;
        $this->slug = $slug;
    }

    public function getName():string {
        return $this->name;
    }

    public function getSlug():string {
        return $this->slug;
    }
}
