<?php
namespace Core\Entity;

use Ramsey\Uuid\UuidInterface;
use Symfony\Component\Serializer\Attribute\SerializedName;

trait IdTrait{

    private UuidInterface $id;
    
    #[SerializedName('id')]
    public function getID():UuidInterface {
        return $this->id;
    }
}
