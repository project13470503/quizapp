<?php

namespace Core\Entity;

use Ramsey\Uuid\Uuid;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;
use DateTimeImmutable;
use Core\Exception\QuestionNotInTestException;
use Core\Exception\QuestionAlreadyAnsweredException;

class Test {
    const MIN_PROCENT_FOR_POSITIVE_RESULT = 75;
    use IdTrait;

    private Collection $testQuestions;
    private float $correctAnsweredProcent = 0;
    private int $possiblePoints;
    private int $totalQuestionsCount;
    private int $answeredQuestionsCount = 0;
    private DateTimeImmutable $createdAt;
    private DateTimeImmutable $endTime;
    private User|null $user = null;


    public function __construct(User $user, int $possiblePoints, array $questions) {
        $this->id = Uuid::uuid7();
        $this->testQuestions = new ArrayCollection();

        foreach($questions as $question) {
            $this->testQuestions->add(new TestQuestion($this, $question));
        }

        $this->possiblePoints = $possiblePoints;
        $this->totalQuestionsCount = $this->testQuestions->count();
        $this->createdAt = new DateTimeImmutable();
        $this->endTime =  $this->createdAt->modify('+50 minute');
        $this->user =  $user;
    }

    private function getQuestion(string $questionID):?TestQuestion {
        $questionFound = $this->testQuestions->filter(function(TestQuestion $testQuestion) use($questionID){
            if((string)$testQuestion->getQuestion()->getID() === $questionID) {
                return $testQuestion;
            }
        });

        if(count($questionFound) > 0)
            return $questionFound[0];
        return null;
    }

    public function hasQuestion(string $questionID):bool {
        return $this->getQuestion($questionID) === null ? false : true;
    }

    public function answerQuestion(string $questionID, array $selectedAnswers, int $points) {

        $testQuestion =  $this->getQuestion($questionID);

        if(!$testQuestion) {
            throw new QuestionNotInTestException($questionID, $this->id->toString());
        }

        if($testQuestion->isAnswered()) {
            throw new QuestionAlreadyAnsweredException($questionID);
        }

        $testQuestion->answered();
        $testQuestion->addSelectedAnswerIDs($selectedAnswers);
        $this->correctAnsweredProcent += round(($points / $this->possiblePoints) * 100);
        $this->answeredQuestionsCount++;

        if($this->isFinished()) {
            $this->finish();
        }
    }

    public function hasQuestions() {
        return $this->answeredQuestionsCount !== $this->totalQuestionsCount;
    }

    public function finish() {
        $this->endTime = new DateTimeImmutable();
    }

    public function getStartTime():DateTimeImmutable {
        return $this->createdAt;
    }

    public function getEndTime():DateTimeImmutable {
        return $this->endTime;
    }

    public function getQuestionsAnsweredCount():int {
        return $this->answeredQuestionsCount;
    }

    public function getTotalQuestionsCount():int {
        return $this->totalQuestionsCount;
    }

    public function getPossiblePoints():int {
        return $this->possiblePoints;
    }

    public function getCorrectAnsweredProcent():int {
        return (int)$this->correctAnsweredProcent;
    }

    public function isFinished():bool {
        if($this->createdAt >= $this->endTime || $this->answeredQuestionsCount === $this->totalQuestionsCount) {
            return true;
        }
        return false;
    }

    public function isPositive():bool {
        return $this->correctAnsweredProcent >= self::MIN_PROCENT_FOR_POSITIVE_RESULT;
    }
}
