<?php
namespace Core\Entity;

use Ramsey\Uuid\Uuid;
use JsonSerializable;

class Answer implements JsonSerializable {

    use IdTrait;

    private string $answer;
    private bool $correctness;
    private Question|null $question = null;

    public function __construct(string $answer, bool $correctness)
    {
        $this->id = Uuid::uuid7();
        $this->answer = $answer;
        $this->correctness = $correctness;
    }

    public function getAnswer():string {
        return $this->answer;
    }

    public function isCorrect():bool {
        return $this->correctness;
    }

    public function addQuestion(Question $question) {
        $this->question = $question;
    }

    public function jsonSerialize(): mixed
    {
        return [
            'id' => $this->id,
            'answer' => $this->answer
        ];
    }
}