<?php
namespace Core\Entity;

use Ramsey\Uuid\Uuid;

class TestQuestion
{
    use IdTrait;

    private Test $test;
    private Question|null $question = null;
    private string $questionID;
    private bool $isAnswered = false;
    private array $selectedAnswerIDs = [];

    public function __construct(Test $test, Question $question)
    {
        $this->id = Uuid::uuid7();
        $this->test = $test;
        $this->question = $question;
    }

    public function getQuestion():?Question {
        return $this->question;
    }

    public function answered():void {
        $this->isAnswered = true;
    }

    public function addSelectedAnswerIDs(array $selectedAnswerIDs):void {
        $this->selectedAnswerIDs = $selectedAnswerIDs;
    }

    public function setTest(Test $test) {
        $this->test = $test;
    }

    public function isAnswered():bool {
        return $this->isAnswered;
    }
}
