<?php

namespace Core\Entity;

use Ramsey\Uuid\Uuid;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;
use Core\Exception\QuestionNotInLevelExeption;

class Level {

    use IdTrait;

    private int $totalQuestionsCount;

    private int $questionsAvailableCount;

    private int $questionsAnswerdCount = 0;

    private int $questionsLockedCount = 0;

    private string $name;

    private ?Training $training = null;

    private Collection $trainingQuestions;

    public function __construct(string $name, int $totalQuestionsCount, array $questions, Training $training) {
        $this->id = Uuid::uuid7();
        $questionsCount = count($questions);
        $this->questionsAvailableCount = $questionsCount > 0 ? $questionsCount : 0;
        $this->totalQuestionsCount = $totalQuestionsCount;
        $this->name = $name;
        $this->trainingQuestions = new ArrayCollection();
        foreach($questions as $question) {
            $this->trainingQuestions->add(new TrainingQuestion($this, $question));
        }
        $this->training = $training;
    }

    public function getQuestion(string $questionID):?TrainingQuestion {

       $question =  $this->trainingQuestions->findFirst(fn($key, TrainingQuestion $trainingQuestion)
            => (string)$trainingQuestion->getQuestion()->getID() === $questionID);

        if(!$question) {
            throw new QuestionNotInLevelExeption($questionID, $this->id);
        }

        return $question;
    }

    public function addTrainingQuestion(TrainingQuestion $trainingQuestion):void {
        if($this->trainingQuestions->contains($trainingQuestion)) {
            return;
        }

        $trainingQuestion->setLevel($this);
        $this->trainingQuestions->add($trainingQuestion);
    }

    public function removeTrainingQuestion(TrainingQuestion $trainingQuestion):void {
        if(!$this->trainingQuestions->contains($trainingQuestion)) {
            return;
        }

        $this->trainingQuestions->removeElement($trainingQuestion);
    }

    public function decreaseQuestionsLockedCount():self {
        $this->questionsLockedCount--;
        return $this;
    }

    public function increaseQuestionsLockedCount():self {
        $this->questionsLockedCount++;
        return $this;
    }

    public function increaseQuestionsAnsweredCount():self {
        $this->questionsAnswerdCount++;
        return $this;
    }

    public function decreaseQuestionsAnsweredCount():self {
        $this->questionsAnswerdCount--;
        return $this;
    }

    public function decreaseQuestionsAvailableCount():self {
        $this->questionsAvailableCount--;
        return $this;
    }

    public function increaseQuestionsAvailableCount():self {
        $this->questionsAvailableCount++;
        return $this;
    }

    public function getQuestionsInLevelProcent():int {
        return (int)round(($this->questionsAvailableCount / $this->totalQuestionsCount) * 100);
    }

    public function getQuestionsLockedProcent():int {
        return (int)round(($this->questionsLockedCount / $this->totalQuestionsCount) * 100);
    }

    public function getQuestionsAnsweredProcent():int {
        return (int)round(($this->questionsAnswerdCount / $this->totalQuestionsCount) * 100);
    }

    public function getName():string {
        return $this->name;
    }
}
