<?php

namespace Core\Entity;

use Ramsey\Uuid\UuidInterface;
use Ramsey\Uuid\Uuid;
use DateTimeInterface;
use DateTimeImmutable;
use Core\Exception\PlayerNotInGameException;

class PlayerGameState
{

    const CORRECT_ANSWERED_POINTS = 100;

    use IdTrait;

    private int $score = 0;
    private int $round = 0;
    private array $answeredQuestionIDs = [];
    private ?DateTimeImmutable $answerdAt;
    private ?User $user = null;
    private UuidInterface $userID;

    public function __construct(
        private Game $game,
        User $user
    ){
        $this->id = Uuid::uuid7();
        $this->user = $user;
        $this->userID = $user->getID();
    }

    public function getPlayer():?User {
        return $this->user;
    }

    public function getUserID():UuidInterface {
        return $this->userID;
    }

    public function disconnect() {
        $this->user = null;
    }

    public function connect(User $user) {
        if(!$this->userID->equals($user->getID())) {
            throw new PlayerNotInGameException($user->getID());
        }

        $this->user = $user;
    }

    public function getAnswerdAt():?DateTimeInterface {
        return $this->answerdAt;
    }

    public function getScore():int {
        return $this->score;
    }

    public function getRound():int {
        return $this->round;
    }

    public function nextRound():void {
       $this->round++;
    }

    public function hasFinishedGame():bool {
        if($this->round === 10) {
            return true;
        }

        return false;
    }

    private function increaseScore():void {
        $this->score += self::CORRECT_ANSWERED_POINTS;
    }

    private function decreaseScore():void {
        $newScore = $this->score - self::CORRECT_ANSWERED_POINTS;
        if($newScore > 0) {
            $this->score = $newScore;
            return;
        }

        $this->score = 0;
    }

    public function getAnsweredQuestionIDs():array {
        return $this->answeredQuestionIDs;
    }

    private function addToAnsweredQuestions(string $questionID):void {
        $this->answeredQuestionIDs[] = $questionID;
    }

    public function getUser():?User {
        return $this->user;
    }

    public function hasQuestionAnswered(string $questionID):bool {
        return in_array($questionID, $this->answeredQuestionIDs);
    }

    public function questionWasAnsered(string $questionID, bool $correctness):void {
        $this->nextRound();
        $this->addToAnsweredQuestions($questionID);
        $this->answerdAt = new DateTimeImmutable();

        $correctness ? $this->increaseScore() : $this->decreaseScore();
    }
}
