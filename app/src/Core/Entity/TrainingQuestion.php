<?php
namespace Core\Entity;

use Ramsey\Uuid\Uuid;

class TrainingQuestion
{
    use IdTrait;

    private Level $level;
    private Question|null $question = null;
    private bool $isLocked = false;
    private int $wrongAnsweredCount = 0;

    public function __construct(Level $level, Question $question)
    {
        $this->id = Uuid::uuid7();
        $this->level = $level;
        $this->question = $question;
    }

    public function getQuestion():?Question {
        return $this->question;
    }

    public function lock():void {
        $this->isLocked = true;
    }

    public function unlock():void {
        $this->isLocked = false;
    }

    public function increaseWrongAnsweredCount():self {
        $this->wrongAnsweredCount++;
        return $this;
    }

    public function setLevel(Level $level) {
        $this->level = $level;
    }

    public function isLocked():bool {
        return $this->isLocked;
    }
}
