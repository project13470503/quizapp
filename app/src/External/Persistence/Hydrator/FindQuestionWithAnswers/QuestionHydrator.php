<?php

namespace External\Persistence\Hydrator\FindQuestionWithAnswers;

use External\Persistence\Hydrator\AbstractHydrator;
use Doctrine\ORM\EntityManagerInterface;
use Core\Service\Response\FindQuestionWithAnswersByID\QuestionResponse;


class QuestionHydrator extends AbstractHydrator
{
    public function __construct(EntityManagerInterface $em)
    {
        parent::__construct($em, QuestionResponse::class);
    }
}