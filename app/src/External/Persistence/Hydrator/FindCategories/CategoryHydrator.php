<?php
declare(strict_types=1);


namespace External\Persistence\Hydrator\FindCategories;

use External\Persistence\Hydrator\AbstractHydrator;
use Doctrine\ORM\EntityManagerInterface;
use Core\Service\Response\FindCategories\CategoryResponse;


class CategoryHydrator extends AbstractHydrator
{

    public function __construct(EntityManagerInterface $em)
    {
        parent::__construct($em, CategoryResponse::class);
    }
}