<?php

namespace External\Persistence\Repository;

use Doctrine\Persistence\ManagerRegistry;
use Doctrine\ORM\Query\Parameter;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\DBAL\Types\Types;
use Doctrine\Common\Collections\ArrayCollection;
use DateTimeImmutable;
use Core\Repository\TestRepository;
use Core\Entity\Test;
use Core\Entity\Question;
use Core\Dto\Response\TestResultResponse;

class DoctrineTestRepository implements TestRepository {

    use RepositoryTrait;

    public function __construct(ManagerRegistry $mangerRegistry, private EntityManagerInterface $entityManager)
    {
        $this->setUp($mangerRegistry, Test::class);
    }

    public function findByID(string $testID, string|null $questionID = null):?Test
    {
        $qb = $this->entityManager->createQueryBuilder()
            ->select('t')
            ->from(Test::class, 't')
            ->where('t.id = :testID')
            ->setParameter('testID', $testID);

        if($questionID !== null) {
            $qb->leftJoin('t.testQuestions', 'tq', Join::WITH, 'tq.questionID = :questionID')
                ->addSelect('tq')
                ->setParameter('questionID', $questionID);
        }

        return $qb->getQuery()
        ->getOneOrNullResult();
    }

    public function findAllByUserID(string $userID) {
       return $this->entityRepository->createQueryBuilder('t')
        ->select(sprintf("t.id, ROUND(t.correctAnsweredProcent) as correctAnsweredProcent , t.answeredQuestionsCount as questionsAnsweredCount, t.createdAt, t.endTime, t.totalQuestionsCount", TestResultResponse::class))
        ->where('t.user = :userID')
        ->orderBy('t.createdAt', 'DESC')
        ->setParameter('userID', $userID)
        ->getQuery()
        ->getResult();
    }

    public function userHasRunningTest(string $userID): bool
    {
        $qb = $this->entityRepository->createQueryBuilder('t');

        return $qb->select('COUNT(t.id)')
        ->where('IDENTITY(t.user) = :userID')
        ->andWhere($qb ->expr()->gte('t.endTime', ':currentTime'))
        ->setParameter('currentTime', new DateTimeImmutable(), Types::DATETIME_IMMUTABLE)
        ->setParameter('userID', $userID)
        ->getQuery()
        ->getSingleScalarResult() > 0;
    }

    public function saveOrUpdate(Test $training): void
    {
        $this->entityManager->persist($training);
        $this->entityManager->flush();
        $this->entityManager->clear();
    }

    public function findQuestionsInTest(string $testID, string $userID):array
    {
        $qb = $this->entityManager->createQueryBuilder();

        return $qb->select('q, a')
            ->from(Question::class, 'q')
            ->join('q.testQuestions', 'tq')
            ->join('tq.test', 't')
            ->join('q.answers', 'a')
            ->where('t.id = :testID')
            ->andWhere('IDENTITY(t.user) = :userID')
            ->andWhere('tq.isAnswered = 0')
            ->orderBy('RAND()')
            ->setParameters(new ArrayCollection([
                new Parameter('testID', $testID),
                new Parameter('userID', $userID)
            ]))
            ->getQuery()
            ->getResult();
    }

    public function delete(Test $test):void {
        $this->entityManager->remove($test);
        $this->entityManager->flush();
    }
}
