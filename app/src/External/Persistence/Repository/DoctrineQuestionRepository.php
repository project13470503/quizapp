<?php

namespace External\Persistence\Repository;

use Doctrine\Persistence\ManagerRegistry;
use Doctrine\ORM\Query\ResultSetMappingBuilder;
use Doctrine\ORM\EntityManagerInterface;
use Core\Repository\QuestionRepository;
use Core\Entity\Question;

class DoctrineQuestionRepository implements QuestionRepository {

    use RepositoryTrait;

    public function __construct(
        ManagerRegistry $managerRegistry,
        private EntityManagerInterface $entityManager){
        $this->setUp($managerRegistry, Question::class);
    }

    public function findIDsByCategories(array $categoryIDs):array {
        return $this->entityRepository->createQueryBuilder('q')
            ->select('q')
            ->where('q.category IN (:categoryIDs)')
            ->setParameter('categoryIDs', $categoryIDs)
            ->getQuery()
            ->getResult();
    }

    public function findByIdWithAnswers(string $questionID):?Question {
        return $this->entityRepository->createQueryBuilder('q')
            ->select('q, a')
            ->join('q.answers', 'a')
            ->where('q.id = :questionID')
            ->setParameter('questionID', $questionID)
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function findQuestionsPerCategory(int $questionsPerCategoryLimit, ?int $questionLimit = null):array
    {
        $rsm = new ResultSetMappingBuilder($this->entityManager);
        $rsm->addRootEntityFromClassMetadata(Question::class, 'q');
        $select = $rsm->generateSelectClause(['q']);

        $sql = "SELECT DISTINCT $select
        FROM (
                SELECT
                    id,
                    category_id,
                    question,
                    ROW_NUMBER() OVER(PARTITION BY category_id ORDER BY RAND()) finalRow
                FROM questions
                ORDER BY category_id
            ) q
        WHERE q.finalRow <= :questionsPerCategoryLimit
        ORDER BY RAND()";

        if($questionLimit !== null) {
            $sql = $sql . ' LIMIT ' . $questionLimit;
        }

        return $this->entityManager->createNativeQuery($sql,$rsm)
        ->setParameter('questionsPerCategoryLimit', $questionsPerCategoryLimit)
        ->getResult();
    }

    public function findCorrectAnsweresCountForQuestions(array $questions):int {
        return $this->entityRepository->createQueryBuilder('q')
            ->select('COUNT(a.id)')
            ->innerJoin('q.answers', 'a')
            ->where('q.id IN (:questions)')
            ->andWhere('a.correctness = 1')
            ->setParameter('questions', $questions)
            ->getQuery()
            ->getSingleScalarResult();
    }
}
