<?php

namespace External\Persistence\Repository;

use Iterator;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Doctrine\ORM\Query\Parameter;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\ORM\NoResultException;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Core\Service\Response\FindTrainings\TrainingResponse;
use Core\Repository\TrainingRepository;
use Core\Entity\Training;
use Core\Entity\Question;
use Core\Entity\Category;

class DoctrineTrainingRepository implements TrainingRepository {


    public function __construct(private EntityManagerInterface $entityManager){}


    public function findTrainingsForUser(string $userID)
    {
        return $this->entityManager->createQueryBuilder()
            ->select('t, l, c')
            ->from(Training::class, 't')
            ->join('t.levels', 'l')
            ->join('t.categories', 'c')
            ->orderBy('t.id', 'ASC')
            ->where('t.user = :userID')
            ->setParameter('userID', $userID)
            ->getQuery()
            ->getResult();
    }

    public function trainingExistsWithCategories(string $userID, array $categories):bool {
        $qb = $this->entityManager->createQueryBuilder();

        try {
            $result = $qb->select('t.id')
                ->from(Training::class, 't')
                ->innerJoin('t.categories', 'c')
                ->where('IDENTITY(t.user) = :userID')
                ->groupBy('t.id')
                ->having('SUM(CASE WHEN c.id IN (:categoryIds) THEN 1 ELSE 0 END) = :categoriesCount')
                ->andHaving('COUNT(c.id) = :categoriesCount')
                ->setParameters(new ArrayCollection([
                    new Parameter('userID', $userID),
                    new Parameter('categoryIds', $categories),
                    new Parameter('categoriesCount', count($categories))
                ]))
                ->getQuery()
                ->getSingleScalarResult();

                return true;

        } catch(NoResultException $e) {
            return false;
        }
    }

    public function findQuestionsForLevel(string $trainingID, string $levelID):Iterator
    {
        $qb = $this->entityManager->createQueryBuilder();

        $query = $qb->select('q, a')
            ->from(Question::class, 'q')
            ->join('q.trainingQuestions', 'tq', Join::WITH, 'IDENTITY(tq.level) = :levelID AND tq.isLocked = 0')
            ->join('q.answers', 'a')
            ->join('tq.level', 'l')
            ->where('IDENTITY(l.training) = :trainingID')
            ->orderBy('RAND()')
            ->setParameters(new ArrayCollection([
                new Parameter('levelID', $levelID),
                new Parameter('trainingID', $trainingID)
            ]))
            ->setMaxResults(10);

        $paginator = new Paginator($query);

        return $paginator->getIterator();
    }

    public function findByID(string $trainingID, ?string $questionID = null): ?Training
    {
        $qb = $this->entityManager->createQueryBuilder()
            ->select('t, l')
            ->from(Training::class, 't')
            ->join('t.levels', 'l');

        if($questionID !== null) {
            $qb->leftJoin('l.trainingQuestions', 'tq', Join::WITH, 'IDENTITY(tq.question) = :questionID')
                ->addSelect('tq')
                ->setParameter('questionID', $questionID);
        }

        return $qb->where('t.id = :trainingID')
        ->setParameter('trainingID', $trainingID)
        ->getQuery()
        ->getOneOrNullResult();
    }


    public function saveOrUpdate(Training $training): void
    {
        $this->entityManager->persist($training);
        $this->entityManager->flush();
        $this->entityManager->clear();
    }
}
