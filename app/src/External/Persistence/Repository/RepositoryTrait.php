<?php

namespace External\Persistence\Repository;

use LogicException;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\ORM\EntityRepository;

trait RepositoryTrait {
    private EntityRepository $entityRepository;

    private function setUp(ManagerRegistry $managerRegistry, string $entityClassNamespace) {
        $manager = $managerRegistry->getManagerForClass($entityClassNamespace);
        
        if ($manager === null) {
            throw new LogicException(sprintf(
                'Could not find the entity manager for class "%s". Check your Doctrine configuration to make sure it is configured to load this entity’s metadata.',
                $entityClassNamespace,
            ));
        }
        
        $this->entityRepository = new EntityRepository($manager, $manager->getClassMetadata($entityClassNamespace));
    }
}