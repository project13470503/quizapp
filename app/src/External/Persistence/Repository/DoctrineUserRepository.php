<?php

namespace External\Persistence\Repository;

use Doctrine\ORM\EntityManagerInterface;
use Core\Repository\UserRepository;
use Core\Entity\User;

class DoctrineUserRepository implements UserRepository {

    public function __construct(private EntityManagerInterface $entityManager){}

    public function findByID(string $id):?User {
        return $this->entityManager->getRepository(User::class)->find($id);
    }

    public function existsByEmail(string $email):bool
    {

        $result = $this->entityManager->createQueryBuilder()
            ->select('COUNT(u.id)')
            ->from(User::class, 'u')
            ->where('u.email = :email')
            ->setParameter('email', $email)
            ->getQuery()
            ->getSingleScalarResult();

        return (int) $result > 0;
    }

    public function existsByUsername(string $username):bool
    {

        $result = $this->entityManager->createQueryBuilder()
            ->select('COUNT(u.id)')
            ->from(User::class, 'u')
            ->where('u.username = :username')
            ->setParameter('username', $username)
            ->getQuery()
            ->getSingleScalarResult();

        return (int) $result > 0;
    }

    public function save(User $user): void {
        $this->entityManager->persist($user);
        $this->entityManager->flush();
    }
}
