<?php

namespace External\Persistence\Repository;

use Doctrine\Persistence\ManagerRegistry;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\ORM\QueryBuilder;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\DBAL\Types\Types;
use DateTimeImmutable;
use Core\Repository\GameReposiotry;
use Core\Enum\GameStatus;
use Core\Entity\PlayerGameState;
use Core\Entity\Game;
use Core\Dto\Response\GameResponse;

class DoctrineGameRepository implements GameReposiotry{

    use RepositoryTrait;


    public function __construct(
        ManagerRegistry $managerRegistry,
        private EntityManagerInterface $entityManager
    )
    {
        $this->setUp($managerRegistry, Game::class);
    }


    public function saveOrUpdate(Game $game):void
    {
        $this->entityManager->persist($game);
        $this->entityManager->flush();
    }


    public function findPublicGames():array
    {
        return $this->entityRepository->createQueryBuilder('g')
            ->select(sprintf(
                'NEW %s(g.id, g.playersInGame, g.maxPlayersInGame, g.status)',
                GameResponse::class
            ))
            ->where("g.mode = 'multiplayer'")
            ->andWhere("g.status = 'searching'")
            ->andWhere('g.createdAt >= :dateTime')
            ->andWhere('g.playersInGame > 0')
            ->setParameter(
                'dateTime',
                (new DateTimeImmutable())->modify('- 20 minutes'), Types::DATETIME_IMMUTABLE
            )
            ->getQuery()
            ->getResult();
    }


    public function findByID(string $id):?Game
    {
        return $this->entityRepository->find($id);
    }


    public function findByIDWithPlayers(string $gameID, ?string $questionID = null): ?Game {
        $qb = $this->entityRepository->createQueryBuilder('g');

        $qb = $this->addPlayers($qb, $gameID);

        if($questionID !== null) {
            $qb->leftJoin('g.questions', 'q', Join::WITH, 'q.id = :questionID')
                ->addSelect('q')
                ->setParameter('questionID', $questionID);
        }

        return $qb->getQuery()
            ->getOneOrNullResult();
    }


    public function findByStatus(string $id, string $userID, GameStatus $status):?Game
    {
        $qb =  $this->entityRepository->createQueryBuilder('g');

        $qb =  $this->addPlayers($qb, $id);
       
        if($status === GameStatus::STARTED) {
 
            $qb->andWhere(
                $qb->expr()->orX(
                    $qb->expr()->eq('g.status', ':status'),
                    $qb->expr()->eq('g.status', ':finished')
                )
            )
            ->andWhere($qb->expr()->eq('p.round', 10))
            ->setParameter('finished', GameStatus::FINISHED);
        }
        else {
            $qb->andwhere("g.status = :status");
        }

        return $qb->andWhere($qb->expr()->exists(
            $this->entityManager->createQueryBuilder()
                ->select('1')
                ->from(PlayerGameState::class, 'pg')
                ->where('pg.user = :userID')
                ->andWhere('pg.game = g.id')
        ))
        ->setParameter('status', $status)
        ->setParameter('userID', $userID)
        ->getQuery()
        ->getOneOrNullResult();
    }


    public function findStartedGame(string $gameID, string $userID):?Game
    {
        $qb =  $this->entityRepository->createQueryBuilder('g');

        return $this->addPlayers($qb, $gameID)
        ->addSelect('q, a')
        ->innerJoin('g.questions', 'q')
        ->innerJoin('q.answers', 'a')
        ->andwhere("g.status = :status")
        ->andWhere($qb->expr()->exists(
            $this->entityManager->createQueryBuilder()
                ->select('1')
                ->from(PlayerGameState::class, 'pg')
                ->where('pg.userID = :userID')
                ->andWhere('pg.game = g.id')
        ))
        ->setParameter('status', GameStatus::STARTED)
        ->setParameter('userID', $userID)
        ->getQuery()
        ->getOneOrNullResult();
    }

    private function addPlayers(QueryBuilder $qb, string $gameID):QueryBuilder {
        return $qb->select('g, p, u')
        ->join('g.playerGameStates', 'p')
        ->leftJoin('p.user', 'u')
        ->where('g.id = :gameID')
        ->setParameter('gameID', $gameID);
    }

}
