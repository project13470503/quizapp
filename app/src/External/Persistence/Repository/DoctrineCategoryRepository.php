<?php

namespace External\Persistence\Repository;

use Doctrine\Persistence\ManagerRegistry;
use Core\Repository\CategoryRepository;
use Core\Entity\Category;
use Core\Dto\Response\CategoryResponse;

class DoctrineCategoryRepository implements CategoryRepository{

    use RepositoryTrait;

    public function __construct(ManagerRegistry $managerRegistry){
        $this->setUp($managerRegistry, Category::class);
    }

    public function findAll() {
        return $this->entityRepository->createQueryBuilder('c')
            ->select(sprintf('NEW %s(c.id, c.name, c.slug)', CategoryResponse::class))
            ->getQuery()
            ->getResult();
    }

    public function findByIDs(array $categoryIDs)
    {
        return $this->entityRepository->createQueryBuilder('c')
            ->select('c')
            ->where('c.id IN (:categoryIDs)')
            ->setParameter('categoryIDs', $categoryIDs)
            ->getQuery()
            ->getResult();
    }
}
