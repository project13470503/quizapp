<?php
namespace External\EventListener;

use Doctrine\ORM\Events;
use Doctrine\ORM\Event\PostFlushEventArgs;
use Doctrine\Bundle\DoctrineBundle\Attribute\AsDoctrineListener;
use Core\Messaging\EventRegistry;
use Core\Messaging\EventBus;


#[AsDoctrineListener(event: Events::postFlush, priority: 500, connection: 'default')]
class EventCollector {
    public function __construct(private EventBus $eventBus){}

    public function postFlush(PostFlushEventArgs $args)
    {
        $eventsToDispatch = EventRegistry::popEvents();

        foreach ($eventsToDispatch as $event) {
            $this->eventBus->dispatch($event);
        }
    }
}
