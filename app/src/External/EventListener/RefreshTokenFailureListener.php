<?php
namespace External\EventListener;

use Symfony\Component\HttpFoundation\Response;
use Gesdinet\JWTRefreshTokenBundle\Security\Exception\TokenNotFoundException;
use Gesdinet\JWTRefreshTokenBundle\Security\Exception\MissingTokenException;
use Gesdinet\JWTRefreshTokenBundle\Security\Exception\InvalidTokenException;
use Gesdinet\JWTRefreshTokenBundle\Event\RefreshTokenNotFoundEvent;
use Gesdinet\JWTRefreshTokenBundle\Event\RefreshAuthenticationFailureEvent;
use External\Api\ApiErrorResponse;

class RefreshTokenFailureListener {

    public function onRefreshAuthenticationFailure(RefreshAuthenticationFailureEvent $event):void
    {
        $message = match(get_class($event->getException())) {
            MissingTokenException::class => 'Es wurde kein Refresh Token angegeben',
            TokenNotFoundException::class => 'Der Refresh Token konnte nicht gefunden werden',
            InvalidTokenException::class => 'Der Refresh Token ist ungültig',
            default => 'Der Refresh Token ist ungültig'
        };

        $respone = new ApiErrorResponse($message, status: Response::HTTP_UNAUTHORIZED);

        $event->setResponse($respone);
    }
}
