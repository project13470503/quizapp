<?php
namespace External\EventListener;

use Gesdinet\JWTRefreshTokenBundle\EventListener\LogoutEventListener as BaseLogoutEventListener;
use Gesdinet\JWTRefreshTokenBundle\Model\RefreshTokenManagerInterface;
use Gesdinet\JWTRefreshTokenBundle\Request\Extractor\ExtractorInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Security\Http\Event\LogoutEvent;

class CustomLogoutEventListener extends BaseLogoutEventListener
{

    public function __construct(
        private RefreshTokenManagerInterface $refreshTokenManager,
        private ExtractorInterface $refreshTokenExtractor,
        private string $tokenParameterName,
        private array $cookieSettings,
        private string $logout_firewall_context
    ) {
        parent::__construct(
            $refreshTokenManager,
            $refreshTokenExtractor,
            $tokenParameterName,
            $cookieSettings,
            $logout_firewall_context
        );
    }

    public function onLogout(LogoutEvent $event): void
    {   
        parent::onLogout($event);

        $response = $event->getResponse();
        $response->setContent('Logout successful');

        $response->headers->clearCookie(
            $this->tokenParameterName,
            $this->cookieSettings['path'],
            $this->cookieSettings['domain'],
            $this->cookieSettings['secure'],
            $this->cookieSettings['http_only'],
            $this->cookieSettings['same_site'],
            $this->cookieSettings['partitioned']
        );

       $response->setContent('Logout successful');

       $response->headers->clearCookie('jwt_s', sameSite: 'strict', secure: true);
       $response->headers->clearCookie('jwt_hp', httpOnly: false, secure: true);
    }
}