<?php

namespace External\EventListener;

use Symfony\Component\Validator\Exception\ValidationFailedException;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpFoundation\Response;
use ReflectionClass;
use External\Service\ValidationFailedErrorsBuilder;
use External\Api\ApiErrorResponse;
use Core\Exception\UserAlreadyExistsException;
use Core\Exception\InvalidCategorySelection;
use Core\Exception\ErrorCode;

use function Symfony\Component\DependencyInjection\Loader\Configurator\env;

class ExceptionListener
{
    private bool $isProd;

    public function __construct(private ValidationFailedErrorsBuilder $errorsBuilder, string $app_env){
        $this->isProd = $app_env === 'prod';
    }


    public function onKernelException(ExceptionEvent $event): void
    {
        $exception = $event->getThrowable();

        $response = $this->handleException($exception);

        $event->setResponse($response);
    }


    private function handleException(\Throwable $exception):Response {

        $previousException = $exception->getPrevious();


        if($previousException instanceof ValidationFailedException) {
            $errors = $this->errorsBuilder->build($previousException->getViolations());
            return new ApiErrorResponse(message: 'Invalid Form', status: Response::HTTP_UNPROCESSABLE_ENTITY, errors: $errors);
        }

        $reflection = new ReflectionClass($exception);
        $attributes = $reflection->getAttributes(ErrorCode::class);

        if (!empty($attributes)) {

            $statusCode = $attributes[0]->newInstance()->code;
            $message = $exception->getMessage();
            $errors = [];

            if($exception instanceof UserAlreadyExistsException) {
                $message = 'Invalid Form';
                $errors[$exception->getType()] = $exception->getMessage();
            }

            if($exception instanceof InvalidCategorySelection) {
                $message = 'Invalid Form';
                $errors['categoryIDs'] = $exception->getMessage();
            }

            return new ApiErrorResponse(message: $message, status: $statusCode, errors: $errors);
        }
        
        if($this->isProd) {
            return new ApiErrorResponse(message: 'An unexpected error occurred', status: Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        dd($exception);
    }
}
