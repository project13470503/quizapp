<?php

namespace External\EventListener;

use Symfony\Component\HttpFoundation\Response;
use Lexik\Bundle\JWTAuthenticationBundle\Event\JWTNotFoundEvent;
use Lexik\Bundle\JWTAuthenticationBundle\Event\JWTInvalidEvent;
use Lexik\Bundle\JWTAuthenticationBundle\Event\JWTExpiredEvent;
use Lexik\Bundle\JWTAuthenticationBundle\Event\JWTCreatedEvent;
use External\Api\ApiErrorResponse;

class JwtEventSubscriber
{
    const INVALID_TOKEN_MESSAGE_PART = ', logge dich erneut ein um einen neuen Token anzufordern';

    public function onJWTCreated(JWTCreatedEvent $event):void
    {
        /** @var Core\Entity\User $user */
        $user = $event->getUser();

        $eventPayload = $event->getData();

        $eventPayload['username'] = $user->getUsername();
        $eventPayload['email'] = $user->getEmail();
        $eventPayload['id'] = $user->getID();
        $eventPayload['avatar'] = $user?->getAvatar()?->getAvatarName();

        $event->setData($eventPayload);
    }

    public function onJWTInvalid(JWTInvalidEvent $event):void
    {
        $event->setResponse($this->createErrorResponse('Dein token ist falsch'));
    }

    public function onJWTNotFound(JWTNotFoundEvent $event):void
    {
        $event->setResponse($this->createErrorResponse('Es wurde kein Token übergeben'));
    }

    public function onJWTExpired(JWTExpiredEvent $event):void
    {
        $event->setResponse($this->createErrorResponse('Dein Token ist bereits abgelaufen'));
    }

    private function createErrorResponse(string $message, int $status = Response::HTTP_UNAUTHORIZED):ApiErrorResponse {
        return new ApiErrorResponse($message   . self::INVALID_TOKEN_MESSAGE_PART, status: $status);
    }
}
