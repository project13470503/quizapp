<?php

namespace External\Service;

use Smalot\PdfParser\Parser;  
use RegexIterator;
use InvalidArgumentException;
use DirectoryIterator;
use Core\Service\QuestionDataLoader;

class QuestionAndAnswerDataLoader implements QuestionDataLoader{

    const FAT_TEXT = ['TT5', 'TT6'];
    
    private int $questionID = 0;
    private int $questionIDBefore = 0;

    private string $categoryName;


    private string $question = '';
    private string $answer = '';

    private array $questions = [];

    private bool $questionHasAnswer = false;

    public function __construct(private Parser $parser)
    {

    }

    public function load(string $fielLocation):array {

        if(!str_ends_with($fielLocation, '.pdf')) {
            throw new InvalidArgumentException(sprintf('%s is not a pdf file', $fielLocation));
        }

        if(!file_exists($fielLocation)) {
            throw new InvalidArgumentException(sprintf('no file at location %s', $fielLocation));
        }        

        $pdf = $this->parser->parseFile($fielLocation);

        $this->categoryName = str_replace(['Prüfungsfragen_', '-', '_', '.pdf'], ['', ' ', ' '], preg_replace('/[0-9 ]+/', '', basename($fielLocation)));

        foreach($pdf->getPages() as $key => $page) {
        
            if($key === 0) {
                continue;
            }

            $pageData = $page->getDataTm();
            
            foreach($pageData as $value) {

                $text = trim(str_replace(['‐', "\u{A0}", '   ', '  '], ['-', ''], $value[1]));;
                $fontIdentifier = $value[2];
                $textPosition = $value[0][4];
               
                $question = trim($this->question, ' \'\"');
                
                if($this->newAnswerStarts($text, $textPosition)  && !empty($question))  {
                    $this->questions[$this->categoryName][$this->questionID]['question'] = $question;
                    $this->question = '';
                }

                if(in_array($fontIdentifier, self::FAT_TEXT)) {
                    if($this->newAnswerStarts($text, $textPosition)) {
                        $this->questionHasAnswer = true;
                    }
                    elseif(!$this->questionHasAnswer) {
                        $this->question = $this->question . ' ' . $text;
                    }
                }
                
                if(str_starts_with($textPosition, '70.8') && ctype_digit(substr(trim($text), 0, 1)) && str_ends_with($text, ')')) {
                    $this->questionIDBefore = $this->questionID;
                    $this->questionID = substr($text, 0, strpos($text, ')'));
                    $this->questionHasAnswer = false;
                }

                if($this->newAnswerStarts($text, $textPosition) && (mb_strlen($text) === 1 || ctype_alpha($text[1]))) {
                    
                    if($this->questionID !== $this->questionIDBefore ) {

                        if(!empty(trim($this->answer))) {
                            $this->addAnswer(useQuestionBefore: true);
                        }
                       
                        $this->questionIDBefore = $this->questionID;

                    }else {
                        $this->addAnswer(false);
                    }
                }
                
                if($fontIdentifier !== 'TT4' && $fontIdentifier !== 'TT3' && $this->questionID === $this->questionIDBefore) {
                    $this->answer = $this->answer . ' ' . $text;
                }
            }
        }

        $this->addAnswer(true);
      
        return $this->questions;
    }

    
    public function loadMultiple(array|string $fielLocations):array {

        if(is_array($fielLocations)) {
            foreach($fielLocations as $fielLocation) {
                $this->load($fielLocation);
            }
        }

        if(is_dir($fielLocations)) {
            $dirIterator = new RegexIterator(new DirectoryIterator($fielLocations), "/\\.pdf\$/i");
            
            foreach($dirIterator as $dir) {

                $this->load($dir->getPathname());
      
            }
        }

        return $this->questions; 
    }

    private function newAnswerStarts(string $text, string $textPosition):bool {
        return (str_starts_with($text, '=') || str_starts_with($text, '-')) && str_starts_with($textPosition, '70.');
    }

    private function addAnswer(bool $useQuestionBefore) {
        $questionID = $useQuestionBefore ? $this->questionIDBefore : $this->questionID;
        $this->questions[$this->categoryName][$questionID]['answers'][] = trim($this->answer);
        $this->answer = '';
    }


}
