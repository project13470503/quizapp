<?php
namespace External\Service;

use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Core\Service\UserIdentity;
use Core\Entity\User;

class UserIdentityService implements UserIdentity {

    public function __construct(private TokenStorageInterface $tokenStorage){}
    
    public function getCurrentUser(): User
    {
        if (null === $token = $this->tokenStorage->getToken()) {
            return null;
        }
        
        return $token->getUser();
    }
}