<?php
namespace External\Service;

use Symfony\Component\Validator\ConstraintViolationListInterface;

class ValidationFailedErrorsBuilder
{
    public function build(ConstraintViolationListInterface $list): array
    {
        $errors     = [];
        foreach ($list as $violation){
            $errors[str_replace(['[', ']'], '', $violation->getPropertyPath())] = $violation->getMessage();
        }
        
        return $errors;
    }
}