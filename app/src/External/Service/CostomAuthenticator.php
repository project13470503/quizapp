<?php
namespace External\Service;

use Symfony\Contracts\Translation\TranslatorInterface;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Component\Validator\Exception\ValidationFailedException;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Security\Http\HttpUtils;
use Symfony\Component\Security\Http\Authenticator\Passport\Passport;
use Symfony\Component\Security\Http\Authenticator\JsonLoginAuthenticator;
use Symfony\Component\Security\Http\Authentication\AuthenticationSuccessHandlerInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationFailureHandlerInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\HttpKernel\Exception\UnprocessableEntityHttpException;
use Symfony\Component\HttpFoundation\Request;

class CostomAuthenticator extends JsonLoginAuthenticator
{
    public function __construct(
        private EventDispatcherInterface $eventDispatcher,
        private ValidatorInterface $validator,
        private HttpUtils $httpUtils,
        private UserProviderInterface $userProvider,
        private AuthenticationSuccessHandlerInterface $successHandler,
        private AuthenticationFailureHandlerInterface $failureHandler,
        private ?TranslatorInterface $translator = null,
        private array $options = []
    ) {
        $this->options = array_merge(['username_path' => 'email', 'password_path' => 'password'], $options);
        parent::__construct($httpUtils, $userProvider, $successHandler, $failureHandler, $this->options);

        if($translator) {
            $this->setTranslator($translator);
        }
    }

    public function authenticate(Request $request): Passport
    {
        $values = [
            $this->options['username_path'] => $request->getPayload()->get($this->options['username_path']),
            $this->options['password_path'] => $request->getPayload()->get($this->options['password_path'])
        ];

        $violations = $this->validator->validate($values, new Assert\Collection([
            $this->options['username_path'] => new Assert\NotBlank(message: 'Die E-Mail Adresse darf nicht leer sein'),
            $this->options['password_path'] => new Assert\NotBlank(message: 'Das Passwort darf nicht leer sein'),
        ]));

        if($violations->count() > 0) {
            throw new UnprocessableEntityHttpException(
                'Validation failed.',
                new ValidationFailedException($values, $violations)
            );
        }

        return parent::authenticate($request);
    }
}
