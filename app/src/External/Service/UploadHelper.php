<?php

namespace External\Service;

use Symfony\Component\String\Slugger\SluggerInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Asset\Context\RequestStackContext;
use Psr\Log\LoggerInterface;
use League\Flysystem\UnableToCheckExistence;
use League\Flysystem\FilesystemOperator;
use League\Flysystem\FilesystemException;

class UploadHelper {

    private array $fileNamesToUpload = [];

    public function __construct(
        private SluggerInterface $slugger,
        private RequestStackContext $requestStackContext,
        private LoggerInterface $logger,
        public string $isPrivate,
        private FilesystemOperator $fileSystem,
        private string $uploadedAssetsBaseUrl,
        )
    {}

    public function doseFileExist(string $path) {
        try {
            return $this->fileSystem->fileExists($path);
        } catch(FilesystemException | UnableToCheckExistence) {
            return false;
        }
    }

    public function deleteFile(string $path)
    {
        $result = $this->fileSystem->delete($path);

        if ($result === false) {
            throw new \Exception(sprintf('Error deleting "%s"', $path));
        }
    }

    public function getUniqueFileName(File $file) {

        $originalFilename = ($file instanceof UploadedFile)
            ? $file->getClientOriginalName()
            : $file->getFilename();

        if(isset($this->fileNamesToUpload[$originalFilename])) {
            return $this->fileNamesToUpload[$originalFilename];
        }

        $newFilename = $this->slugger->slug(pathinfo($originalFilename, PATHINFO_FILENAME)).'-'.uniqid().'.'.$file->guessExtension();

        $this->fileNamesToUpload[$originalFilename] = $newFilename;

        return $newFilename;
    }

    public function uploadFile(File $file, string $directory, ?string $existingFilename = null): string {

        $this->deleteImageIfExists($directory . $existingFilename);

        $newFilename = $this->getUniqueFileName($file);

        $this->move($file, $directory . '/' . $newFilename);

        return $newFilename;
    }

    private function move(File $file, $fullFilePath) {
        $stream = fopen($file->getPathname(), 'r');

        $result = $this->fileSystem->writeStream(
            $fullFilePath,
            $stream
        );

        if ($result === false)
            throw new \Exception(sprintf('Could not write uploaded file "%s"', $fullFilePath));

        if (is_resource($stream))
            fclose($stream);
    }

    private function deleteImageIfExists(?string $existingFilename):void {
        if (!$existingFilename) {
            return;
        }

        try {
            $result = $this->fileSystem->delete($existingFilename);

            if ($result === false)
                throw new \Exception(sprintf('Could not delete old uploaded file "%s"', $existingFilename));

        } catch (\Exception $e) {
            $this->logger->alert(sprintf('Old uploaded file "%s" was missing when trying to delete', $existingFilename));
        }
    }

    public function readStream(string $path)
    {
        $resource = $this->fileSystem->readStream($path);

        if ($resource === false)
            throw new \Exception(sprintf('Error opening stream for "%s"', $path));

        return $resource;
    }
}
