<?php

namespace External\Service;

use Lexik\Bundle\JWTAuthenticationBundle\Services\KeyLoader\KeyLoaderInterface;

class JwtEnvKeyLoader implements KeyLoaderInterface 
{
    const PUBLIC_KEY_NAME = 'JWT_PUBLIC_KEY';
    const PRIVATE_KEY_NAME = 'JWT_SECRET_KEY';
    const PASSPHRASE_NAME = 'JWT_PASSPHRASE';

    public function __construct(private ?string $signingKey = null, private ?string $publicKey = null, private ?string $passphrase = null, array $additionalPublicKeys = [])
    {
        $this->signingKey = getenv(static::PRIVATE_KEY_NAME);
        $this->publicKey = getenv(static::PUBLIC_KEY_NAME);
        $this->passphrase = getenv(static::PASSPHRASE_NAME);
        
        if (!$this->signingKey) {
            throw new \RuntimeException(sprintf('Signing key from environment variable "%s" is not set.', $signingKey));
        }

        if (!$this->publicKey) {
            throw new \RuntimeException(sprintf('Public key from environment variable "%s" is not set.', $publicKey));
        }
    }

    public function loadKey($type):string
    {
        if (!in_array($type, [self::TYPE_PUBLIC, self::TYPE_PRIVATE])) {
            throw new \InvalidArgumentException(sprintf('The key type must be "public" or "private", "%s" given.', $type));
        }

        return self::TYPE_PUBLIC === $type ? $this->getPublicKey() : $this->getSigningKey();
    }

    public function getPassphrase():?string 
    {
        return $this->passphrase;
    }

    public function getSigningKey():?string 
    {
        return $this->signingKey;
    }

    public function getPublicKey():?string 
    {
        return $this->publicKey;
    }

    public function getAdditionalPublicKeys():array
    {
        return [];
    }
}