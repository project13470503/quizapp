<?php

namespace External\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\HttpKernel\KernelInterface;
use Doctrine\ORM\EntityManagerInterface;
use Core\Service\QuestionDataLoader;
use Core\Entity\Category;
use Core\Entity\Answer;
use Core\Entity\Question;
use Ramsey\Uuid\Uuid;

class FillDatabaseCommand extends Command
{
    public function __construct(
        private QuestionDataLoader $dataLoader, 
        private EntityManagerInterface $entityManager,
        private KernelInterface $kernel)
    {
        parent::__construct();
    }

    protected function configure()
    {
        $this->setName('app:fill-database');
        $this->setDescription('Füllt die Datenbank mit Initialdaten.');
        $this->setHelp('Füllt die Datenbank mit Kategorien Fragen und Antworten basierend auf pdf datein');
    }

    protected function execute(InputInterface $input, OutputInterface $output):int
    {
        (int) $categoryCount = $this->entityManager->createQueryBuilder()
            ->select('COUNT(c.id)')
            ->from(Category::class, 'c')
            ->getQuery()
            ->getSingleScalarResult();

        if($categoryCount > 0) {
            return Command::SUCCESS;
        }

        $data = $this->dataLoader->loadMultiple($this->kernel->getProjectDir() . '/questions/');
        
        foreach($data as $categoryName => $questions) {
            $slug = str_replace(' ', '-' ,strtolower($categoryName));
            $categoryEntity = new Category($categoryName, $slug);
            $categoryID = Uuid::uuid7();

            $this->entityManager->persist($categoryEntity);

            foreach($questions as $questionNumber =>  $question) {                
                $questionEntity = new Question($question['question'], $categoryEntity);

                $this->entityManager->persist($questionEntity);

                foreach($question['answers'] as $answer) {
    
                    $answerString = trim(substr($answer, 1));
                    
                    $correctness = ($answer[0] === '=') ? true : false;
                    
                    $answerEntity = new Answer($answerString, $correctness);
                    $answerEntity->addQuestion($questionEntity);
                    $this->entityManager->persist($answerEntity);
                }
            }
        }

        $this->entityManager->flush();

        $output->writeln('Datenbank erfolgreich befüllt.');

        return Command::SUCCESS;
    }
}