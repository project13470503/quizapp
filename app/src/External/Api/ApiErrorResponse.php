<?php

namespace External\Api;

use Symfony\Component\HttpFoundation\JsonResponse;

class ApiErrorResponse extends JsonResponse{
    public function __construct(string $message = '', array $errors = [], int $status = 200, array $headers = [], bool $json = false)
    {

        $responseBody = [
            'message' => $message,
            'status' => $status
        ];

        if(!empty($errors)) {
            $responseBody['errors'] = $errors;
        }

        parent::__construct($responseBody, $status, $headers, $json);

        $this->setEncodingOptions(JSON_UNESCAPED_UNICODE);
    }
}
