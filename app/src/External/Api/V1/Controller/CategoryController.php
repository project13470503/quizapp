<?php
namespace External\Api\V1\Controller;

use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use OpenApi\Attributes as OA;
use Core\Service\CategoryService;


class CategoryController extends AbstractController {

    public function __construct(
        private readonly CategoryService $categoryService
    ){}

    /**
    * List all Categories
    */
    #[Route('api/v1/categories', 'api.category.index', methods: ['GET'])]
    public function index(Request $request) {

        $categories = $this->categoryService->findAllCategories();

        return $this->json($categories);
    }

}
