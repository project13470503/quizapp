<?php
namespace External\Api\V1\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Liip\ImagineBundle\Service\FilterService;

class ImageController extends AbstractController {

    public function displayImage(
        Request $request,
        string $filter, string
        $path,
        FilterService $filterService
    ):RedirectResponse|BinaryFileResponse
    {
        $file = $filterService->getUrlOfFilteredImage(
            $path,
            $filter,
            webpSupported: $this->isWebpSupported($request)
        );

        if(str_contains($filter, 'private'))
            return $this->file($file);

        return $this->redirect($file);
    }


    private function isWebpSupported(Request $request): bool
    {
        return false !== mb_stripos($request->headers->get('accept', ''), 'image/webp');
    }
}
