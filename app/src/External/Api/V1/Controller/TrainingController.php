<?php
namespace External\Api\V1\Controller;

use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\HttpKernel\Attribute\MapRequestPayload;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Core\Service\TrainingService;
use Core\Dto\Request\StartTrainingDto;
use Core\Dto\Request\AnswerQuestionInTrainingDto;

class TrainingController extends AbstractController{


    public function __construct(private TrainingService $trainingService){}


    #[Route('api/v1/trainings', 'api.training.index', methods: ['GET'])]
    public function index() {

        $trainings = $this->trainingService->getTrainingsForUser();

        return $this->json($trainings , context: [AbstractNormalizer::GROUPS => 'findTrainings']);
    }


    #[Route('api/v1/trainings', 'api.training.start', methods: ['POST'])]
    public function startTraining(#[MapRequestPayload()]StartTrainingDto $dto) {

        $response = $this->trainingService->startTraining($dto);

        return $this->json($response, Response::HTTP_CREATED);
    }


    #[Route('api/v1/trainings/{trainingID}/levels/{levelID}/questions', 'api.training.questions', methods: ['GET'])]
    public function getQuestionList(Request $request, string $trainingID, string $levelID) {

        $questions = $this->trainingService->getQuestions($trainingID, $levelID);

        return $this->json($questions);
    }


    #[Route('api/v1/trainings/answer', 'api.training.answerQuestion', methods: ['PATCH'])]
    public function answerQuestion(#[MapRequestPayload()]AnswerQuestionInTrainingDto $dto) {

        $response = $this->trainingService->checkCorrectness($dto);

        return $this->json($response);
    }
}
