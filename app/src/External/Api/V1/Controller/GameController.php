<?php
namespace External\Api\V1\Controller;

use Symfony\Component\Serializer\Normalizer\AbstractObjectNormalizer;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\HttpKernel\Attribute\MapRequestPayload;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Core\Service\GameService;
use Core\Dto\Request\PrepareGameDto;
use Core\Dto\Request\AnswerQuestionInGameDto;
use Core\Enum\GameStatus;

class GameController extends AbstractController {

    public function __construct(private GameService $gameService){}


    #[Route('api/v1/games', 'api.game.prepare', methods: ['POST'], format:'json')]
    public function prepareGame(#[MapRequestPayload(acceptFormat:'json')]PrepareGameDto $dto) {

        $gameID = $this->gameService->prepareGame($dto);

        return $this->json([
            'gameID' => $gameID
        ], Response::HTTP_CREATED);
    }


    #[Route('api/v1/games/join', 'api.game.join', methods: ['PATCH'])]
    public function joinGame(Request $request) {
        $gameID = $request->getPayload()->get('gameID');

        $hasStarted = $this->gameService->join($gameID);

        return $this->json([
            'hasStarted' => $hasStarted
        ]);
    }


    #[Route('api/v1/games/start', 'api.game.start', methods: ['PATCH'])]
    public function startGame(Request $request) {
        $gameID = $request->getPayload()->get('gameID');

        $this->gameService->start($gameID);

        return $this->json(['success' => 'Successfuly started the game']);
    }


    #[Route('api/v1/games/answerQuestion', 'api.game.answer', methods: ['PATCH'])]
    public function answerQuestion(#[MapRequestPayload(acceptFormat:'json')]AnswerQuestionInGameDto $dto) {

        $this->gameService->checkCorrectness($dto);

        return $this->json(['message' => 'Answered question in game']);
    }


    #[Route('api/v1/games', 'api.game.index', methods: ['GET'])]
    public function getPublicGames() {

        $games = $this->gameService->getPublicGames();

        return $this->json($games, Response::HTTP_OK);
    }


    #[Route('api/v1/games/{id}/{status}', 'api.game.show', methods: ['GET'])]
    public function getGame(string $id, GameStatus $status) {

        $game = $this->gameService->findGame($id, $status);
   
        return $this->json($game, context: [
            AbstractNormalizer::GROUPS => $status === GameStatus::SEARCHING ? 'showMultiplayerGame' : 'showFinishedGame'
        ]);
    }


    #[Route('api/v1/games/{gameID}/questions', 'api.game.questions', methods: ['GET'], priority: 1)]
    public function findStartedGame(string $gameID) {

        $game = $this->gameService->findStartedGame($gameID);

        return $this->json($game, context: [
            'groups' => 'questions',
            AbstractObjectNormalizer::SKIP_UNINITIALIZED_VALUES => true
        ]);
    }

    #[Route('api/v1/games/leave', 'api.game.leave', methods: ['PATCH'])]
    public function leaveGame(Request $request) {

        $gameID = $request->getPayload()->get('gameID');

        $this->gameService->leave($gameID);

        return $this->json(['success' => 'Du hast das Spiel verlassen']);
    }
}
