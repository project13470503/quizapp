<?php
namespace External\Api\V1\Controller;

use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\HttpKernel\Attribute\MapRequestPayload;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Core\Service\TestService;
use Core\Dto\Request\FinishTestDto;
use Core\Dto\Request\AnswerQuestionInTestDto;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;

class TestController extends AbstractController{

    public function __construct(private TestService $testService){}

    #[Route('api/v1/tests', 'api.test.index', methods: ['GET'])]
    public function index() :JsonResponse
    {
        return $this->json($this->testService->getTestsForUser());
    }


    #[Route('api/v1/tests', 'api.test.start', methods: ['POST'])]
    public function startTest() :JsonResponse
    {
        return $this->json(
            $this->testService->startTest(),
            Response::HTTP_CREATED
        );
    }


    #[Route('api/v1/tests/answer', 'api.test.answerQuestion', methods: ['PATCH'])]
    public function answerQuestion(#[MapRequestPayload(acceptFormat:'json')]AnswerQuestionInTestDto $dto) :JsonResponse
    {
        return $this->json($this->testService->answerQuestionInTests($dto));
    }


    #[Route('api/v1/tests/finish', 'api.test.finish', methods: ['PATCH'])]
    public function finish(#[MapRequestPayload(acceptFormat:'json')]FinishTestDto $dto):JsonResponse
    {
        $response = $this->testService->finish($dto);

        return $this->json($response);
    }


    #[Route('api/v1/tests/{testID}/questions', 'api.test.questions', methods: ['GET'])]
    public function getQuestions(string $testID):JsonResponse
    {
        return $this->json($this->testService->findQuestionsForTest($testID));
    }

    #[Route('api/v1/tests/{testID}', 'api.test.show', methods: ['GET'])]
    public function getTest(string $testID):JsonResponse
    {
        return $this->json($this->testService->findTest($testID), context: [
            AbstractNormalizer::GROUPS => 'findTest'
        ]);
    }

    #[Route('api/v1/tests/{testID}', 'api.test.delete', methods: ['DELETE'])]
    public function delete(string $testID):JsonResponse
    {
        $this->testService->delete($testID);

        return $this->json(["success" => "Test wurde gelöscht"]);
    }
}
