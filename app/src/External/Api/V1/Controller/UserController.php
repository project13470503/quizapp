<?php
namespace External\Api\V1\Controller;


use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\HttpKernel\Attribute\MapRequestPayload;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use External\Service\ValidationFailedErrorsBuilder;
use External\Service\UploadHelper;
use External\Api\ApiErrorResponse;
use Core\Service\UserService;
use Core\Entity\Avatar;
use Core\Dto\Request\RegisterUserDto;

class UserController extends AbstractController {

    public function __construct(
        private readonly UserService $userService
    ){}

    #[Route('/api/v1/users/register', 'api.register', methods: ['POST'], format:'json')]
    public function register(#[MapRequestPayload(acceptFormat:'json', validationFailedStatusCode: 422)] RegisterUserDto $registerUserDto):Response {

        $this->userService->signUp($registerUserDto);

        return $this->json(
            data:["success" => $registerUserDto->username .  " wurde erfolgreich registriert!"],
            status: 201
        );
    }


    #[Route('api/v1/users/uploadAvatar', 'api.uploadAvatar', methods: ['POST'], format:'json')]
    public function uploadAvatar(
        UploadHelper $privateFileUploader,
        ValidationFailedErrorsBuilder $errorBuilder,
        ValidatorInterface $validator,
        Request $request
    ){

        $avatar = $request->files->get('avatar');

        $violations = $validator->validate(
            value: ['avatar' => $avatar],
            constraints: new Assert\Collection([
                'avatar' => [
                    new Assert\NotNull(message:'Bitte wähle ein Bild aus um einen neuen Avatar festzulegen'),
                    new Assert\Image(maxSize: '1M')
                ],
            ])
        );

        if($violations->count() > 0) {
            return new ApiErrorResponse(
                'Invalid Form',
                $errorBuilder->build($violations),
                Response::HTTP_UNPROCESSABLE_ENTITY
            );
        }

        $newFileName = $privateFileUploader->getUniqueFileName($avatar);

        $currentAvatar = $this->userService->addAvatar($newFileName);

        $privateFileUploader->uploadFile($avatar, Avatar::AVATAR_PATH, $currentAvatar);

        return $this->json(
            data:[
                'data' => [
                    'avatar' => Avatar::AVATAR_PATH . $newFileName
                ],
                'success' => 'Das Profilbild wurde erfolgreich hochgeladen'
            ],
            status: 201
        );
    }

}
