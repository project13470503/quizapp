<?php

namespace External\Messaging;

use Symfony\Component\Messenger\Stamp\DelayStamp;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Messenger\Envelope;
use Core\Messaging\EventBus;
use Core\Messaging\Event;
use Core\Event\QuestionCorrectAnsweredEvent;
use Symfony\Component\Messenger\Stamp\BusNameStamp;
use Symfony\Component\Messenger\Stamp\TransportNamesStamp;

class SymfonyEventBus implements EventBus {

    private $halfANHourInMiliSeconds = 60000 * 30;

    public function __construct(private MessageBusInterface $eventBus){}

    public function dispatch(Event $event): void
    {
        $stamps = [];

        if( $event instanceof QuestionCorrectAnsweredEvent) {
            $stamps[] = new DelayStamp($this->halfANHourInMiliSeconds);

            $stamps[] = new TransportNamesStamp('async');
            $stamps[] = new BusNameStamp('event.bus');
        }else {
            $stamps[] = new TransportNamesStamp('zeromq');
            $stamps[] = new BusNameStamp('message.bus');
        }


        try{
            $this->eventBus->dispatch(new Envelope($event,  $stamps));
        } catch(\Exception $e) {
            dd($e->getMessage());
        }

    }
}
