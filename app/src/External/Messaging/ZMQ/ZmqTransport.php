<?php

namespace External\Messaging\ZMQ;

use Symfony\Component\Messenger\Transport\TransportInterface;
use Symfony\Component\Messenger\Envelope;
use External\Messaging\ZMQ\ZmqSender;
use External\Messaging\ZMQ\ZmqReceiver;

class ZmqTransport implements TransportInterface
{
    public function __construct(
        private ZmqSender $sender,
        private ZmqReceiver $receiver
    ){}

    public function send(Envelope $envelope): Envelope
    {
        return $this->sender->send($envelope);
    }

    public function get(): iterable
    {
        return $this->receiver->get();
    }

    public function reject(Envelope $envelope): void {}

    public function ack(Envelope $envelope): void {}
}
