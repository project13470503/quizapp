<?php
namespace External\Messaging\ZMQ;

use ZMQException;
use ZMQContext;
use Symfony\Component\Serializer\SerializerInterface as SymfonySerializer;
use Symfony\Component\Serializer\Exception\UnexpectedValueException;
use Symfony\Component\Serializer\Encoder\EncoderInterface;
use Symfony\Component\Serializer\Encoder\DecoderInterface;
use Symfony\Component\Messenger\Transport\Receiver\ReceiverInterface;
use Symfony\Component\Messenger\Stamp\SerializedMessageStamp;
use Symfony\Component\Messenger\Exception\TransportException;
use Symfony\Component\Messenger\Exception\MessageDecodingFailedException;
use Symfony\Component\Messenger\Envelope;
use Psr\Log\LoggerInterface;

class ZmqReceiver implements ReceiverInterface
{
    private $socket = null;
    private $shouldStop = false;

    public function __construct(
        private LoggerInterface $logger,
        private SymfonySerializer $serializer,
        private EncoderInterface $encoder,
        private DecoderInterface $decoder,
        private string $dsn
    ){}

    private function bind() {
      try {
          $context = new ZMQContext();
          $this->socket = $context->getSocket(\ZMQ::SOCKET_PULL);
          $this->socket->bind($this->dsn);
      } catch (ZMQException $e) {
          $this->logger->critical('Failed to create ZMQ context or socket: ' . $e->getMessage());
          throw new TransportException('Unable to establish ZMQ Receiver.', 0, $e);
      }
    }

    public function get(): iterable
    {

        if ($this->socket === null) {
            $this->bind();
        }

        while (!$this->shouldStop) {
            try {
                $jsonMessaage = $this->socket->recv();
        
                $messageArray = $this->decoder->decode($jsonMessaage, 'json');
  
                if (empty($messageArray['body']) || empty($messageArray['headers']))
                    throw new MessageDecodingFailedException('Encoded envelope should have at least a "body" and some "headers", or maybe you should implement your own serializer.');
   
                if (empty($messageArray['headers']['type']))
                    throw new MessageDecodingFailedException('Encoded envelope does not have a "type" header.');
    
                $message = $this->serializer->deserialize(
                    $this->encoder->encode($messageArray['body'], 'json'),
                    $messageArray['headers']['type'],
                    'json'
                );
                
                yield new Envelope($message, [
                  new SerializedMessageStamp($this->encoder->encode($messageArray['body'], 'json'))
                ]);
            }
            catch(MessageDecodingFailedException | UnexpectedValueException $e) {
                $this->logger->error('Failed to receive message: ' . $e->getMessage());
                continue;
            }
            catch (\ZMQSocketException $e) {
                if ($e->getCode() === 4) { 
                    $this->logger->info('Worker abgebrochen (SIGINT).');
                    exit(0);
                }
                $this->logger->error('Failed to receive message: ' . $e->getMessage());
                continue;
            }
        }
    }

    public function ack(Envelope $envelope): void {}

    public function reject(Envelope $envelope): void {}

    public function __destruct()
    {
        if ($this->socket !== null) {
            try {
                $this->socket->disconnect($this->dsn);
            } catch (ZMQException $e) {
                $this->logger->error('Failed to unbind or close ZMQ socket: ' . $e->getMessage());
            }
        }
    }
}
