<?php
namespace External\Messaging\ZMQ;

use ZMQException;
use ZMQContext;
use UnexpectedValueException;
use Symfony\Component\Serializer\Encoder\EncoderInterface;
use Symfony\Component\Messenger\Transport\Sender\SenderInterface;
use Symfony\Component\Messenger\Stamp\SerializedMessageStamp;
use Symfony\Component\Messenger\Exception\TransportException;
use Symfony\Component\Messenger\Envelope;
use Psr\Log\LoggerInterface;

class ZmqSender implements SenderInterface
{
    private $socket = null;

    public function __construct(
        private LoggerInterface $logger,
        private EncoderInterface $encoder,
        private string $dsn
    ){}

    private function connect(): void
    {
        try {
            $context = new ZMQContext();
            $this->socket = $context->getSocket(\ZMQ::SOCKET_PUSH);
            $this->socket->connect($this->dsn);
        } catch (ZMQException $e) {
            $this->logger->critical('Failed to create ZMQ context or socket: ' . $e->getMessage());
            throw new TransportException('Unable to establish ZMQ Semder.', 0, $e);
        }
    }

    public function send(Envelope $envelope): Envelope
    {

        if ($this->socket === null) {
            $this->connect();
        }

        try {

            $serializedStamp = $envelope->last(SerializedMessageStamp::class);

            if(!$serializedStamp) {
                $serializedMessage = $this->encoder->encode($envelope->getMessage(), 'json');

                $envelope = $envelope->with(new SerializedMessageStamp($serializedMessage));
            }

            $this->socket->send(
              $envelope->last(SerializedMessageStamp::class)->getSerializedMessage()
            );
        } catch(UnexpectedValueException $e) {
            $this->logger->error('Failed to encode the message: ' . $e->getMessage());
            throw new TransportException('Could not send message.', 0, $e);
        } catch (\ZMQSocketException $e) {
            $this->logger->error('Failed to send message: ' . $e->getMessage());
            throw new TransportException('Could not send message.', previous: $e);
        }

        return $envelope;
    }
}
