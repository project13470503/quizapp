<?php

namespace External\Messaging\ZMQ;

use Symfony\Component\Serializer\SerializerInterface as SymfonySerializerInterface;
use Symfony\Component\Serializer\Encoder\EncoderInterface;
use Symfony\Component\Serializer\Encoder\DecoderInterface;
use Symfony\Component\Messenger\Transport\TransportInterface;
use Symfony\Component\Messenger\Transport\TransportFactoryInterface;
use Symfony\Component\Messenger\Transport\Serialization\SerializerInterface;
use Symfony\Component\Messenger\Bridge\Amqp\Transport\Connection;
use Symfony\Component\Messenger\Bridge\Amqp\Transport\AmqpTransport;
use Psr\Log\LoggerInterface;
use InvalidArgumentException;
use External\Messaging\ZMQ\ZmqSender;
use External\Messaging\ZMQ\ZmqReceiver;

class TransportFactory implements TransportFactoryInterface
{
    public function __construct(
        private LoggerInterface $logger,
        private SymfonySerializerInterface $serializer,
        private EncoderInterface $encoder,
        private DecoderInterface $decoder
    ){}

    public function createTransport(string $dsn, array $options, SerializerInterface $serializer): TransportInterface
    {
        $scheme = parse_url($dsn, PHP_URL_SCHEME);

        return match($scheme) {
            'zmq' => $this->createZmqTransport($dsn),
            'amqp' => $this->createAmqpTransport($dsn, $options, $serializer),
            default => throw new InvalidArgumentException(sprintf('The transport "%s" is not supported.', $scheme))
        };
    }

    private function createZmqTransport(string $dsn):ZmqTransport {

        $dsnParts = $this->extractTransportDsn($dsn);

        $senderDsn = $dsnParts['sender'] ?? null;
        $receiverDsn = $dsnParts['receiver'] ?? null;

        if ($receiverDsn === null || $senderDsn === null)
            throw new \InvalidArgumentException('Both receiver and sender DSN must be provided for ZeroMQ.');


        return new ZmqTransport(
            new ZmqSender(
                $this->logger,
                $this->encoder,
                $senderDsn
            ),
            new ZmqReceiver(
                $this->logger,
                $this->serializer,
                $this->encoder,
                $this->decoder,
                $receiverDsn
            )
        );
    }

    private function createAmqpTransport(string $dsn, array $options, SerializerInterface $serializer):AmqpTransport {
        unset($options['transport_name']);

        return new AmqpTransport(Connection::fromDsn($dsn, $options), $serializer);
    }

    private function extractTransportDsn(string $dsn):array
    {
            $result = [];

            $dsn = str_replace('zmq://', '', $dsn);

            $parts = explode(',', $dsn);

            foreach ($parts as $part) {
                $part = trim($part);
                if (strpos($part, 'sender:') === 0) {
                    $result['sender'] = substr($part, strlen('sender:'));
                } elseif (strpos($part, 'receiver:') === 0) {
                    $result['receiver'] = substr($part, strlen('receiver:'));
                }
            }

            return $result;
    }

    public function supports(string $dsn, array $options): bool
    {
        return str_starts_with($dsn, 'zmq:') || str_starts_with($dsn, 'amqp:');
    }
}
