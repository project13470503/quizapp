<?php

namespace External\DataFixtures;


use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Core\Service\QuestionDataLoader;
use Core\Entity\Question;
use Core\Entity\Category;
use Core\Entity\Answer;

class QuestionFixtures extends Fixture
{

    public function __construct(private QuestionDataLoader $dataLoader)
    {
    }

    public function load(ObjectManager $manager): void
    {
        $data = $this->dataLoader->loadMultiple(dirname(__DIR__, 3) . "/questions/");
        
        foreach($data as $categoryName => $questions) {
            $slug = str_replace(' ', '-' ,strtolower($categoryName));
            $categoryEntity = new Category($categoryName, $slug);

            $manager->persist($categoryEntity);

            foreach($questions as $questionNumber =>  $question) {                
                $questionEntity = new Question($question['question'], $categoryEntity);

                $manager->persist($questionEntity);

                foreach($question['answers'] as $answer) {
    
                    $answerString = trim(substr($answer, 1));
                    
                    $correctness = ($answer[0] === '=') ? true : false;
                    
                    $answerEntity = new Answer($answerString, $correctness);
                    $answerEntity->addQuestion($questionEntity);
                    $manager->persist($answerEntity);
                }
            }
        }

        $manager->flush();
    }

}
