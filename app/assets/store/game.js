import GameAPI from "./../api/game.js";

function getAvatarUrl(avatar) {
  return (!avatar)
    ? env.WEBSITE_URL + '/images/user.png'
    : env.API_URL + 'images/squared_thumbnail_small_private/' + avatar;
}

export default {
  namespaced: true,
  state: {
    games: [],
    currentGame: null,
    error: null,
  },
  getters: {
    hasError(state) {
      return state.error !== null;
    },
    error(state) {
      return state.error;
    },
    games(state) {
      return state.games;
    },
    hasGames(state) {
      return state.games.length > 0;
    },
    getGame(state) {
      return state.currentGame;
    },
    hasGame(state) {
      return state.currentGame !== null
    },
    hasQuestions(state) {
      if (state.currentGame?.hasOwnProperty('questions') && state.currentGame?.questions?.length > 0)
        return true;

      return false;
    },
    getNextQuestion(state, getters) {
      return (playerID) => {
        const currentPlayer = state.currentGame?.players?.find(player => player.id === playerID);

        if (!currentPlayer || !getters.hasQuestions) return false;
  
        return state.currentGame.questions.find(question =>
          !currentPlayer.answeredQuestionIDs.includes(question.id)
        ) ?? false;
      }
    },
    hasPlayerFinishedGame(state) {
      return (playerID) => {
        const currentPlayer = state.currentGame?.players?.find(player => player.id === playerID);

        if (!currentPlayer) return false;

        return currentPlayer.round === 10;
      };
    }
  },
  mutations: {
    fetchGamesSuccess(state, games) {
      state.error = null;
      state.games = games;
    },
    fetchGameSuccess(state, game) {

      game.players = game.players.map((player) => {
      
        player.user.avatar = getAvatarUrl(player.user.avatar?.avatarName);

        if(player?.score  !== undefined) {
          player.user.score = player.score;
        }
        return player.user;
      });

      state.currentGame = game;
    },
    fetchPlayingGameSuccess(state, game) {
      let date = new Date(Date.parse(game.createdAt));
      date.setSeconds(date.getSeconds() + 41);
      game.players = game.players.map((player) => {

        if ((date < new Date() || player.round !== 0) && player.answerdAt !== null) {
          date = new Date(Date.parse(player.answerdAt));
        }

        

        return {
          round: player.round,
          score: player.score,
          answeredQuestionIDs: player.answeredQuestionIDs,
          username: player.user?.username ?? `Bot`,
          id: player.userID,
          avatar: getAvatarUrl(player.user?.avatar?.avatarName),
          answeredAt: date
        };
      });

      state.currentGame = game;
    },
    gamePrepared(state, game) {
      state.currentGame = game;
      state.games.unshift(game);
    },
    playerJoinedGame(state, data) {

      if(state.currentGame !== null) {
        state.currentGame.playersInGame++;
        data.player.avatar = getAvatarUrl(data.player.avatar?.avatarName);
        if(state.currentGame?.players) {
          state.currentGame.players.push(data.player);
        }
      }

      const joinedGameInList =  state.games.find(gameInList => gameInList.id === data.gameID);

      if(!joinedGameInList) return;

      joinedGameInList.playersInGame++;

      if(joinedGameInList.playersInGame === joinedGameInList.maxPlayersInGame) {
        state.games = state.games.filter(gameInList => gameInList.id !== joinedGameInList.id);
      }
      
    },
    playerLeftGame(state, data) {
      const gameInList = state.games.find(game => game.id === data.gameID);
      const playerInGame = state.currentGame?.players?.find(player => player.id === data.playerID);
      
      if (state.currentGame && playerInGame) {
        if (playerInGame.id === data.loggedInUser) {
          state.currentGame = null;
        } else if (state.currentGame.hasOwnProperty('questions')) {
          playerInGame.username = 'Bot';
          playerInGame.avatar = getAvatarUrl(null);
        } else {
          state.currentGame.playersInGame--;
          state.currentGame.players = state.currentGame.players.filter(player => player.id !== data.playerID);
        }
      }
      
      if(gameInList) {
        gameInList.playersInGame--;

        if(gameInList.playersInGame === 0) {
          state.games = state.games.filter(game => game.id !== data.gameID);
        }
      }
    },
    gameStarted(state, gameID) {
      if(state.currentGame) {
        state.currentGame.status = 'started';
      }

      state.games = state.games.filter((game) => game.id !== gameID);
    },
    playerAnsweredQuestion(state, data) {
      const currentPlayer = state.currentGame.players.find(player => player.id === data.playerID);

      const date = new Date(Date.parse(data.answeredAt));
      date.setSeconds(date.getSeconds() + 40);
      currentPlayer.score = data.score;
      currentPlayer.answeredQuestionIDs.push(data.questionID);
      currentPlayer.round++
      currentPlayer.answeredAt = date;
    },
    playerConnectedToGame(state, data) {
      const currentPlayer = state.currentGame?.players?.find(player => player.id === data.playerID);

      if (!currentPlayer) return;

      currentPlayer.username = data.username;
      currentPlayer.avatar = getAvatarUrl(data.avatar);
    },
    playerFinishedGame(state, player) {
      const currentPlayer = state.currentGame?.players?.find(playerInGame => playerInGame.id === player.playerID);
  
      if (!currentPlayer) {
        player.round = 10;
        state.currentGame?.players.push(player);
        return;
      }

      currentPlayer.round = 10;
      currentPlayer.score = player.score;
    },
    error(state, error) {
      state.error = error;
    }
  },
  actions: {
    async fetchSearchingGames({ commit }) {
      try {
        const response = await GameAPI.fetchGamesSearchingForPlayers();
        commit('fetchGamesSuccess', response);
        return response;
      } catch (error) {
        commit('error', error);
        return null;
      }
    },
    async fetchGame({ commit }, data) {
      try {
        const response = await GameAPI.fetchGame(data.gameID, data.status);
        commit('fetchGameSuccess', response);
        return response;
      } catch (error) {
        commit('error', error);
        return null;
      }
    },
    async fetchPlayingGame({ commit }, data) {
      try {
        const response = await GameAPI.fetchPlayingGame(data.gameID);
        response.currentPlayer = data.player;
        commit('fetchPlayingGameSuccess', response);
        return response;
      } catch (error) {
        commit('error', error);
        return null;
      }
    },
    async prepareGame({ commit }, request) {
      try {
        const response = await GameAPI.prepareGame(request);
        commit('error', null);
        return response;
      } catch (error) {
        commit('error', error);
        return null;
      }
    },
    async joinGame({ commit }, gameID) {
      try {
        const response = await GameAPI.join(gameID);
        commit('error', null);
        return response;
      } catch (error) {
        commit('error', error);
        return null;
      }
    },
    async leaveGame({ commit }, gameID) {
      try {
        const response = await GameAPI.leave(gameID);
        commit('error', null);
        return response;
      } catch (error) {
        commit('error', error);
        return null;
      }
    },
    async startGame({ commit }, gameID) {
      try {
        const response = await GameAPI.start(gameID);
        commit('error', null);
        return response;
      } catch (error) {
        commit('error', error);
        return null;
      }
    },
    async answerQuestion({ commit }, request) {
      try {
        const response = await GameAPI.answerQuestion(request);
        commit('error', null);
        return response;
      } catch (error) {
        commit('error', error);
        return null;
      }
    },
    SOCKET_gamePrepared: (context, message) => {
      const game = JSON.parse(message);
      game.playersInGame = 1;
      game.status = 'searching';
      context.commit('gamePrepared', game);
    },
    SOCKET_playerJoinedGame({ commit }, message) {
      commit('playerJoinedGame', JSON.parse(message));
    },
    SOCKET_playerLeftGame({ commit, rootGetters  }, message) {
      const data = JSON.parse(message);
      data.loggedInUser = rootGetters['auth/getUser'].id;
      commit('playerLeftGame', data);
    },
    SOCKET_gameStarted({ commit }, message) {
      commit('gameStarted', JSON.parse(message).id);
    },
    SOCKET_playerAnsweredQuestion({ commit, getters }, message) {
      const data = JSON.parse(message);
      data.questionID = getters.getNextQuestion(data.playerID).id;

      commit('playerAnsweredQuestion', data);
    },
    SOCKET_playerConnectedToGame({ commit }, message) {
      commit('playerConnectedToGame', JSON.parse(message));
    },
    SOCKET_playerFinishedGame({ commit }, message) {
      commit('playerFinishedGame', JSON.parse(message));
    },
  }
};
