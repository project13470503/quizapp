
export default {
    namespaced: true,
    state: {
        id: null,
        levelID: null,
        questions: []
    },
    getters: {
        getID(state) {
            return state.id;
        },
        getLevelID(state) {
            return state.levelID;
        },
        getQuestions(state) {
            return state.questions;
        },
        hasQuestions(state) {
            return (questionCount = 0) => state.questions.length > questionCount;
        },
        getNextQuestionID(state) {
            return (currentQuestionID) => {
                return state.questions.find((question) => question !== currentQuestionID) ?? null;
            }
        }
    },
    mutations: {
        trainingStarted(state, trainingData) {
            state.id = trainingData.trainingID;
            state.levelID = trainingData.levelID;
        },
        testStarted(state, test) {
            state.id = test.id;
            state.questions = test.questionIDs;
            state.levelID = null;
        },
        gameStarted(state, id, questions) {
            state.id = id;
            state.questions = questions
            state.levelID = null;
        },
        newQuestionPool(state, questions) {
            questions.map((question) => {
                if (!state.questions.includes(question)) state.questions.push(question);
            });
        },
        questionAnswered(state, questionID) {
            state.questions = state.questions.filter((question) => questionID != question);
        }
    },
    actions: {
        startTraining({ commit }, trainingData) {
            commit('trainingStarted', trainingData)
        },
        startTest({ commit }, response) {
            commit('testStarted', response)
        },
        startGame({ commit }, gameID) {
            commit('gameStarted', gameID)
        },
        updateQuestions({ commit }, questions) {
            commit('newQuestionPool', questions);
        },
        questionAnswered({ commit }, questionID) {
            commit('questionAnswered', questionID);
        }
    }
};
