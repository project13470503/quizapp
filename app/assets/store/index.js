import Vuex from "vuex";
import Auth from "./auth/auth";
import Loader from "./loading/loader";
import Flash from "./flash/flash";
import Training from "./training/training";
import Category from "./category/category";
import Test from "./test";
import Modal from "./modal";
import Game from "./game";

export default new Vuex.Store({
    modules: {
        auth: Auth,
        loader: Loader,
        flash: Flash,
        training: Training,
        category: Category,
        test: Test,
        modal: Modal,
        game: Game
    }
});
