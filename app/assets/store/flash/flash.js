export default {
    namespaced: true,
    state: {
        message: null,
        type: null,
        time: null
    },
    actions: {
        show({ commit }, flash) {
            commit("show", flash);
        },
        hide({ commit }) {
            commit("hide");
        }
    },
    getters: {
        message(state) {
            return state.message;
        },
        type(state) {
            return state.type;
        },
        hasFlash(state) {
            return state.message !== null;
        },
        time(state) {
            return state.time;
        }
    },
    mutations: {
        show(state, flash) {
            state.message =  flash.message,
            state.type =  flash.type
            if(flash.hasOwnProperty('time') && flash.time !== null) state.time = flash.time
        },
        hide(state) {
            state.message = null,
            state.type = null
            state.time = null
        }
    }
};