import AuthAPI from "../../api/auth";
import Cookies from 'js-cookie';

export default {
    async login({ commit }, payload) {
        try {
            await AuthAPI.login(payload.email, payload.password);
            commit('authSuccess', Cookies.get('jwt_hp'));
        } catch (error) {
            commit('authError', error);
            return null;
        }
    },
    async logout({ commit }) {
        try {
            Cookies.remove('jwt_hp');
            Cookies.remove('jwt_s');
            commit('logout');
            const response = await AuthAPI.logout();
            return response;
        } catch (error) {
            return null;
        }
    },
    async signUp({ commit }, payload) {
        try {
            let response = await AuthAPI.signUp(payload);
            commit('signUpSuccess');
            return response;
        } catch (error) {
            commit('signUpError', error);
            return null;
        }
    },
    async uploadAvatar({ commit }, payload) {
        try {
            const response = await AuthAPI.uploadProfileImage(payload);
            commit('avatarUploaded', response.data.avatar);
            return response;
        } catch (error) {
            commit('error', error);
            return null;
        }
    },
    async refreshToken({ commit }, callback) {
        try {
            const response = await AuthAPI.refreshToken();
            commit('authSuccess', Cookies.get('jwt_hp'));
            return response;
        } catch (error) {
            if (callback) {
                callback();
            }
            return null;
        }
    }
}
