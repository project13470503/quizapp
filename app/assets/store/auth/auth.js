import actions from './actions';
import getters from './getters';
import mutations from './mutations';
import Cookies from 'js-cookie';

export default {
  namespaced: true,
  state: {
    error: null,
    isAuthenticated: Cookies.get('jwt_hp') !== undefined ? true : false,
    token: Cookies.get('jwt_hp'),
    avatar: null
  },
  getters: getters,
  mutations: mutations,
  actions: actions,
  watch: {
    token: function (newToken) {
        console.log('token changed ', newToken);
        this.isAuthenticated = newToken !== undefined && newToken !== null;
    }
}
}
