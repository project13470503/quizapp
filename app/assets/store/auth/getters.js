export default {
  hasError(state) {
    return state.error !== null;
  },
  error(state) {
    return state.error;
  },
  isAuthenticated(state) {
    return state.isAuthenticated;
  },
  getUser(state) {
    return JSON.parse(atob(state.token?.split('.')[1]));
  },
  getAvatar(state) {
    return state.avatar;
  }
}
