export default {
  authSuccess(state, token) {
    state.error = null;
    state.isAuthenticated = true;
    state.token = token;
  },
  authError(state, error) {
    state.error = error;
    state.isAuthenticated = false;
    state.token = null;
  },
  error(state, error) {
    state.error = error;
  },
  avatarUploaded(state, avatar) {
    state.avatar = avatar;
    state.error = null;
    state.isAuthenticated = true;
  },
  logout(state) {
    state.error = null;
    state.isAuthenticated = false;
    state.token = null;
  },
  signUpSuccess(state) {
    state.error = null;
    state.isAuthenticated = false;
    state.token = null;
  },
  signUpError(state, error) {
    state.error = error;
    state.isAuthenticated = false;
    state.token = null;
  }
}
