import TrainingAPI from "../../api/training";

export default {
    async fetchTrainings({ commit }) {
        try {
            const response = await TrainingAPI.fetchAll();

            commit('fetchTrainingsSuccess', response);
            return response;
        } catch (error) {
            return null;
        }
    },
    async answerQuestion({ commit }, payload) {
        try {
            const response = await TrainingAPI.answerQuestion(payload);
            response.answerdQuestionID = payload.questionID;
            response.trainingID = payload.trainingID;
            response.levelID = payload.levelID;

            commit('answerdQuestion', response);

            return response;
        } catch (error) {
            commit('answerdQuestionError', error);
            return null;
        }
    },
    async startTraining({ commit }, dto) {

        try {
            const response = await TrainingAPI.start({
                categoryIDs: dto.categoryIDs
            });

            const { trainingID, levelID } = response;

            const newTraining = createTraining(
                trainingID,
                levelID,
                dto.categories
            );

            commit('startTrainingSuccess', newTraining);

            return response;
        } catch (error) {
            commit('startTrainingError', error);
            return null;
        }
    },
    async fetchQuestionForLevel({ commit }, data) {
        try {
            const questions = await TrainingAPI.fetchQuestionsForLevel(data.trainingID, data.levelID);

            commit('fetchQuestionSuccess', { questions: questions, levelID: data.levelID });
            return questions;
        } catch (error) {
            commit('fetchQuestionError', error);
            return null;
        }
    },
    continueLevel({ commit }, data) {
        commit('continueLevel', data);
    }
}

function createTraining(trainingID, levelID, categories) {
    return {
        id: trainingID,
        levels: [
            {
                "id": levelID,
                "name": "Level 1",
                "questionsAnsweredProcent": 0,
                "questionsLockedProcent": 0,
                "questionsInLevelProcent": 100
            },
            {
                "id": "",
                "name": "Level 2",
                "questionsAnsweredProcent": 0,
                "questionsLockedProcent": 0,
                "questionsInLevelProcent": 0
            },
            {
                "id": "",
                "name": "Level 3",
                "questionsAnsweredProcent": 0,
                "questionsLockedProcent": 0,
                "questionsInLevelProcent": 0
            }
        ],
        categories: categories
    };
}
