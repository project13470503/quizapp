export default {
    fetchQuestionSuccess(state, questions) {
        state.error = null;
        state.levelQuestions.set(questions.levelID, questions.questions);
    },
    fetchQuestionError(state, error) {
        state.error = error;
        state.levelQuestions = [];
    },
    fetchTrainingsSuccess(state, trainings) {
        state.error = null;
        state.trainings = trainings;
    },
    startTrainingSuccess(state, training) {
        state.error = null;
        state.trainings.unshift(training);
    },
    startTrainingError(state, error) {
        state.error = error;
    },
    answerdQuestion(state, result) {
        if (state.levelQuestions.has(result.levelID)) {
            const filteredQuestions = state.levelQuestions
                .get(result.levelID)
                .filter((question) => question.id !== result.answerdQuestionID);

            state.levelQuestions.set(result.levelID, filteredQuestions);
        }

        state.error = null;

        const currentTraining = state.trainings.find(training => training.id === result.trainingID);

        if (currentTraining) {
            currentTraining.levels = result.levelProgress;
        }
    },
    answerdQuestionError(state, error) {
        state.error = error;
    }
}
