import actions from './actions';
import getters from './getters';
import mutations from './mutations';

export default {
    namespaced: true,
    state: {
        trainings: [],
        error: null,
        levelQuestions: new Map()
    },
    getters: getters,
    mutations: mutations,
    actions: actions
};
