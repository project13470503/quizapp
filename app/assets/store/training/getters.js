export default {
  hasTrainings(state) {
    return state.trainings.length > 0;
  },
  trainings(state) {
    return state.trainings;
  },
  hasError(state) {
    return state.error !== null;
  },
  error(state) {
    return state.error;
  },
  hasQuestions(state) {
    return (levelID) => {
      if (state.levelQuestions.has(levelID)) {
        return state.levelQuestions.get(levelID).length > 0
      }
      return false;
    };
  },
  getNextQuestion(state) {
    return (levelID) => {
      if (!state.levelQuestions.has(levelID)) {
        return null;
      }

      if (state.levelQuestions.get(levelID).length === 0) {
        return null;
      }

      return state.levelQuestions.get(levelID)[0];
    }
  }
}
