import TestAPI from "./../api/test";

function createTest(response) {

    const startTime = new Date(response.startTime);
    const endTime = new Date(startTime.getTime() + 50 * 60000);

    return {
        id: response.id,
        correctAnsweredProcent: 0,
        questionsAnsweredCount: 0,
        totalQuestionsCount: response.totalQuestionsCount,
        status: 'started',
        createdAt: response.startTime,
        endTime: endTime
    };
}

export default {
    namespaced: true,
    state: {
        tests: [],
        error: null,
    },
    getters: {
        hasError(state) {
            return state.error !== null;
        },
        error(state) {
            return state.error;
        },
        tests(state) {
            return state.tests;
        },
        hasTests(state) {
            return state.tests.length > 0;
        },
        test: (state) => (identifier) => {
            return state.tests.find(test => {
                if (test.id === identifier) return { test }
            });
        },
        getNextQuestion(state) {
            return state.tests[0].questions[0];
        },
        hasQuestions(state) {
            const currentTest = state.tests[0];
            if (currentTest && currentTest.hasOwnProperty('questions') && currentTest.questions.length > 0) {
                return true;
            }
            return false;
        },
        getTotalQuestionCount(state) {
            return state.tests[0].totalQuestionsCount;
        },
        getQuestionsAnsweredCount(state) {
            return state.tests[0].questionsAnsweredCount;
        },
        getCorrectAnsweredProcent(state) {
            return state.tests[0].correctAnsweredProcent;
        }
    },
    mutations: {
        fetchTestsSuccess(state, tests) {
            state.error = null;
            state.tests = tests;
        },
        startTestSuccess(state, test) {
            state.error = null;
            state.tests.unshift(test);
        },
        answerQuestionSuccess(state, result) {
            state.error = null;
            //handle isfinished on answering
            state.tests[0].correctAnsweredProcent = result.currentResult;
            state.tests[0].questionsAnsweredCount = result.questionsAnsweredCount;
            state.tests[0].questions = state.tests[0].questions.filter((question) => question.id !== result.questionID);
        },
        fetchQuestionSuccess(state, questions) {
            state.tests[0].questions = questions;
        },
        fetchTestSuccess(state, test) {
            state.tests.unshift(test);
        },
        testEnded(state, result) {
            state.error = null;
            state.tests[0].status = 'finished';
            state.tests[0].correctAnsweredProcent = result.correctAnsweredProcent;
            state.tests[0].endTime = result.endTime;
        },
        error(state, error) {
            state.error = error;
        }
    },
    actions: {
        async fetchTests({ commit }) {
            try {
                const response = await TestAPI.fetchAll();
                commit('fetchTestsSuccess', response);
                return response;
            } catch (error) {
                commit('error', error);
                return null;
            }
        },
        async startTest({ commit }) {
            try {
                const response = await TestAPI.start();
                const test = createTest(response);
                commit('startTestSuccess', test);
                return response;
            } catch (error) {
                commit('error', error);
                return null;
            }
        },
        async answerQuestion({ commit }, payload) {
            try {
                const response = await TestAPI.answer(payload);
                response.questionID = payload.questionID;
                commit('answerQuestionSuccess', response);
                return response;
            } catch (error) {
                commit('error', error);
                return null;
            }
        },
        async fetchQuestionsForTest({ commit }, testID) {
            try {
                const response = await TestAPI.fetchQuestionsForTest(testID);
                commit('fetchQuestionSuccess', response);
                return response;
            } catch (error) {
                commit('error', error);
                return null;
            }
        },
        async fetchTest({ commit }, testID) {
            try {
                const response = await TestAPI.fetchTest(testID);
                commit('fetchTestSuccess', response);
                return response;
            } catch (error) {
                commit('error', error);
                return null;
            }
        },
        async finish({ commit }, testID) {
            try {
                const response = await TestAPI.finish({
                    id: testID
                });
                commit('testEnded', response);
                return response;
            } catch (error) {
                commit('error', error);
                return null;
            }
        },
    }
};
