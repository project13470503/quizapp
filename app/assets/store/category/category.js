import CategoryAPI from "../../api/category";

export default {
    namespaced: true,
    state: {
        categories: [],
        error: null,
    },
    getters: {
        hasError(state) {
            return state.error !== null;
        },
        error(state) {
            return state.error;
        },
        categories(state) {
            return state.categories;
        }
    },
    mutations: {
        fetchCategoriesSuccess(state, categories) {
            state.error = null;
            state.categories = categories;
        },
        fetchCategoriesFailure(state, error) {
            state.error = error;
            state.categories = [];
        }
    },
    actions: {
        async fetchCategories({ commit }) {
            try {
                const response = await CategoryAPI.fetchAll();
                commit('fetchCategoriesSuccess', response);
                return response;
            } catch (error) {
                commit('fetchCategoriesFailure', error);
                return null;
            }
        }
    }
};