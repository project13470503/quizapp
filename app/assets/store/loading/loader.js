export default {
    namespaced: true,
    state: {
        loaders: []
    },
    actions: {
        show({ commit }, identifier) {
            commit("show", identifier);
        },
        hide({ commit }, identifier) {
            commit("hide", identifier);
        }
    },
    getters: {
        isLoading: (state) => (identifier) => {
            return state.loaders.includes(identifier);
        }
    },
    mutations: {
        show(state, identifier) {
            state.loaders.push(identifier);
        },
        hide(state, identifier) {
            state.loaders = state.loaders.filter((loader) => {
                if(loader != identifier) return loader;
            });
        }
    }
};