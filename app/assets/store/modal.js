import { Modal } from 'bootstrap';

export default {
  namespaced: true,
  state: {
    modals: new Map,
  },
  getters: {
    isOpen(state) {
      return (modalName) => {
        return state.modals.has(modalName);
      };
    }
  },
  mutations: {
    open(state, modalName) {

      if (!state.modals.has(modalName)) {
        state.modals.set(modalName, new Modal('#' + modalName));
      }

      state.modals.get(modalName).show();
    },
    close(state, modalName) {

      if (!state.modals.has(modalName)) return;

      if (state.modals.get(modalName)._isShown) {
        state.modals.get(modalName).hide();
      }

      state.modals.delete(modalName);
    }
  },
  actions: {
    open({ commit }, modalName) {
      commit('open', modalName);
    },
    close({ commit }, modalName) {
      commit('close', modalName);
    },

  }
};
