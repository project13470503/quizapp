import { createApp } from 'vue'
import router from './router';
import Vuex from 'vuex';
import store from './store';
import App from './App.vue';
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome';
import './styles/app.scss';
import './icons.js';
import SocketIOPlugin from './plugins/SocketIO.js'

const app = createApp(App);

app.use(router)
    .use(Vuex)
    .use(store)
    .use(SocketIOPlugin)
    .mount('#app');

app.config.productionTip = false;
app.component('FontAwesomeIcon', FontAwesomeIcon)
