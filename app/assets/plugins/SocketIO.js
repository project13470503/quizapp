import mixin from '../mixin.js';
import store from '../store/index.js';
import VueSocketIO from 'vue-socket.io';
import SocketIO from 'socket.io-client';

const socketPlugin = new VueSocketIO({
  debug: true,
  connection: SocketIO(process.env.WEBSITE_URL, {
    withCredentials: true,
    autoConnect: false,
    reconnection: true,
    reconnectionDelay: 2000,
    reconnectionDelayMax: 5000,
    reconnectionAttempts: 5,
    secure: true
  }),
  vuex: {
    store,
    actionPrefix: 'SOCKET_',
    mutationPrefix: 'SOCKET_'
  }
});

socketPlugin.install = (function (app) {
  app.config.globalProperties.$socket = this.io;
  app.provide('socket', this.io);
  app.config.globalProperties.$vueSocketIo = this
  app.mixin(mixin);
}).bind(socketPlugin);

export default socketPlugin;
