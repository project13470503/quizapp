import { httpClient } from './httpClient';

export default {
    fetchAll() {
        return httpClient.get("/tests");
    },
    start() {
        return httpClient.post("/tests");
    },
    answer(payload) {
        return httpClient.patch("/tests/answer", payload);
    },
    finish(payload) {
        return httpClient.patch("/tests/finish", payload);
    },
    fetchQuestionsForTest(testID) {
        return httpClient.get(`tests/${testID}/questions`);
    },
    fetchTest(testID) {
        return httpClient.get(`tests/${testID}`);
    }
}
