import { httpClient } from './httpClient';

export default {
    fetchAll() {
        return httpClient.get("/categories");
    }
}