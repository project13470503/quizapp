import { httpClient } from './httpClient';

export default {
    login(email, password) {
        return httpClient.post("users/login", {
            email: email,
            password: password
        });
    },
    signUp(user) {
        return httpClient.post("users/register", user);
    },
    uploadProfileImage(data) {
        return httpClient.post(`users/uploadAvatar`, data, {
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded',
                'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8'
            }
        });
    },
    refreshToken() {
        return httpClient.post('users/token/refresh', {}, {
            withCredentials: true
        });
    },
    logout() {
        return httpClient.post('users/logout', {}, {
            withCredentials: true
        });
    }
}
