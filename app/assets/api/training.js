import { httpClient } from './httpClient';

export default {
    fetchAll() {
        return httpClient.get("/trainings");
    },
    start(payload) {
        return httpClient.post("/trainings", payload);
    },
    answerQuestion(payload) {
        return httpClient.patch("/trainings/answer", payload);
    },
    fetchQuestionsForLevel(trainingID, levelID) {
        return httpClient.get(`trainings/${trainingID}/levels/${levelID}/questions`);
    }
}
