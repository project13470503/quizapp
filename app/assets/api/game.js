import { httpClient } from './httpClient';

export default {
  fetchGamesSearchingForPlayers() {
    return httpClient.get("games");
  },
  fetchGame(gameID, status) {
    return httpClient.get(`games/${gameID}/${status}`);
  },
  fetchPlayingGame(gameID) {
    return httpClient.get(`games/${gameID}/questions`);
  },
  prepareGame(request) {
    return httpClient.post("games", request);
  },
  join(gameID) {
    return httpClient.patch("games/join", {
      gameID: gameID
    });
  },
  leave(gameID) {
    return httpClient.patch("games/leave", {
      gameID: gameID
    });
  },
  start(gameID) {
    return httpClient.patch("games/start", {
      gameID: gameID
    });
  },
  answerQuestion(request) {
    return httpClient.patch("games/answerQuestion", request);
  },
}
