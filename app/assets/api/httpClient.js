import axios from "axios";

const httpClient = axios.create({
    baseURL: process.env.API_URL
});

function getRequestIdentifier(config) {

    if (!config.url.startsWith('/')) {
        return config.method + '/' + config.url;
    }

    return config.method + config.url;
}

function setupInterceptors({ dispatch }, { push }) {
    const req = {
        pending: (loaderIdentifier) => {
            dispatch('loader/show', loaderIdentifier);
        },
        done: (loaderIdentifier) => {
            dispatch('loader/hide', loaderIdentifier);
        }
    };

    httpClient.interceptors.request.use(
        config => {
            req.pending(getRequestIdentifier(config));
            config.withCredentials = true;
            return config;
        },
        error => {
            req.done(getRequestIdentifier(error.config));
            return Promise.reject(error);
        }
    );

    httpClient.interceptors.response.use((response) => {
        req.done(getRequestIdentifier(response.config));
        return Promise.resolve(response.data);
    },
        async error => {
            req.done(getRequestIdentifier(error.config));

            if (error.config.hasOwnProperty("errorHandle") &&
                error.config.errorHandle === false) {
                return Promise.reject(error.response);
            }

            if (error.response.status) {
                switch (error.response.status) {
                    case 400:
                        console.log('Client Error', error.response.data);
                        break;
                    case 401:
                        const routesToLogout = ["users/token/refresh", "users/token/logout"];

                        if (!routesToLogout.includes(error.config.url)) {
                            const response = await dispatch('auth/refreshToken', () => push({ name: 'login' }));
                            
                             if (response) {
                                 const originalRequest = { ...error.config };
                                 originalRequest.withCredentials = true;
                                 httpClient.request(originalRequest);
                             }
                             break;
                        }

                        dispatch('auth/logout');
                        push({ name: 'login' });
                        break;
                    case 404:
                        console.log('Not Found Error', error.response.data);
                        break;
                    case 500:
                        console.log('Server Error', error.response.data);
                }
            }

            return Promise.reject(error.response);
        });
}

export { httpClient, setupInterceptors };
