import { createRouter, createWebHistory } from 'vue-router';
import Home from '../views/Home.vue';
import LoginForm from '../views/auth/LoginForm.vue';
import SignUpForm from '../views/auth/SignUpForm.vue';
import UploadAvatar from '../views/auth/UploadAvatar.vue';
import TestList from '../views/test/TestList.vue';
import TestQuestion from '../views/test/TestQuestion.vue';
import TrainingList from '../views/training/TrainingList.vue';
import TrainingQuestion from '../views/training/TrainingQuestion.vue';
import GameList from '../views/game/GameList.vue';
import StartGame from '../views/game/StartGame.vue';
import GameQuestion from '../views/game/GameQuestion.vue';
import GameResult from '../views/game/GameResult';

const routes = [
    {
        path: '/',
        name: 'home',
        component: Home
    },
    {
        path: '/login',
        name: 'login',
        component: LoginForm
    },
    {
        path: '/signup',
        name: 'signup',
        component: SignUpForm
    },
    {
        path: '/user/changeAvatar',
        name: 'changeAvatar',
        component: UploadAvatar
    },
    {
        path: '/trainings',
        name: 'trainings',
        component: TrainingList
    },
    {
        path: '/tests',
        name: 'tests',
        component: TestList
    },
    {
        path: '/trainings/:trainingID/levels/:levelID/answerQuestion',
        name: 'trainingQuestion',
        component: TrainingQuestion,
        props: true
    },
    {
        path: '/tests/:id/answerQuestion',
        name: 'testQuestion',
        component: TestQuestion,
        props: true
    },
    {
        path: '/games',
        name: 'games',
        component: GameList
    },
    {
        path: '/games/:id/start',
        name: 'startGame',
        component: StartGame
    },
    {
        path: '/games/:id/questions',
        name: 'gameQuestion',
        component: GameQuestion
    },
    {
        path: '/games/:id/result',
        name: 'gameResult',
        component: GameResult
    }
];

const router = createRouter({
    history: createWebHistory('/'),
    routes,
    linkActiveClass: "active",
    linkExactActiveClass: "active"
});

export default router;
