function stringifyValues(object = {}) {
    return Object.entries(object).reduce(
        (acc, curr) => ({ ...acc, [`${curr[0]}`]: JSON.stringify(curr[1]) }),
        {}
    );
}

const costomEnvConfig = {
    ...stringifyValues({
        API_URL: "https://quizapp.space.local/api/v1/",
        WEBSITE_URL: "https://quizapp.space.local",
        SOCKET_URL: "https://quizapp.space.local",
    })
};

const Encore = require('@symfony/webpack-encore');

if (!Encore.isRuntimeEnvironmentConfigured()) {
    Encore.configureRuntimeEnvironment(process.env.NODE_ENV || 'dev');
}

Encore
    .setOutputPath('./public/build')
    .setPublicPath('/build')
    .configureDefinePlugin(options => {
        options.__VUE_OPTIONS_API__ = true;
        options.__VUE_PROD_DEVTOOLS__ = false;
        options.__VUE_PROD_HYDRATION_MISMATCH_DETAILS__ = false;
        options.process = { env: costomEnvConfig };
        options.env = costomEnvConfig;
    })
    .addEntry('app', './assets/app.js')
    .enableVueLoader()
    .splitEntryChunks()
    .disableSingleRuntimeChunk()
    .cleanupOutputBeforeBuild()
    .enableBuildNotifications()
    .enableSourceMaps(!Encore.isProduction())
    .enableVersioning(Encore.isProduction())
    .configureBabelPresetEnv((config) => {
        config.useBuiltIns = 'usage';
        config.corejs = '3.23';
    })
    .enableSassLoader()
    .autoProvidejQuery();

module.exports = Encore.getWebpackConfig();
