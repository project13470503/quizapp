# Quiz-Training


## How to install

Run following commands

```
git clone git@gitlab.com:project13470503/quizapp.git
cd quizapp

docker compose up -d --build
docker exec -it php832-container sh

In php docker container run following commands:


composer dump-autoload
composer update

php bin/console doctrine:database:create
php bin/console doctrine:migrations:diff
php bin/console doctrine:migrations:migrate
php bin/console doctrine:fixtures:load

php bin/console lexik:jwt:generate-keypair

npm install --global yarn
corepack enable
yarn install
yarn add @fortawesome/vue-fontawesome
yarn dev
```
