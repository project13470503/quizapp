DROP USER IF EXISTS `root`@`%`;

DROP USER IF EXISTS 'db_user'@'%';

CREATE USER 'db_user'@'db_host' REQUIRE SSL;

GRANT SELECT, INSERT, UPDATE, DELETE ON `db_name`.* TO 'db_user'@'db_host';