#!/bin/bash
set -e

. /usr/local/bin/loadEnvVars.sh

sed -i "s/db_user/${MYSQL_USER}/g" /docker-entrypoint-initdb.d/init.sql
sed -i "s/allowed_ip/${ALLOWED_IP}/g" /docker-entrypoint-initdb.d/init.sql
sed -i "s/db_password/${MYSQL_PASSWORD}/g" /docker-entrypoint-initdb.d/init.sql
sed -i "s/db_name/${MYSQL_DATABASE}/g" /docker-entrypoint-initdb.d/init.sql

exec docker-entrypoint.sh "$@"