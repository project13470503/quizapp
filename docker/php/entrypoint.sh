#!/bin/bash

set -e

BUILD_TARGET="${BUILD_TARGET:-dev}"

MIGRATIONS_DIR="./src/External/Persistence/Migration"
MIGRATIONS_NAMESPACE="External\Persistence\Migration"

if [ "$BUILD_TARGET" = "prod" ]; then
    php bin/console cache:clear --env=prod --no-debug
else
    php bin/console cache:clear
fi


php bin/console doctrine:database:create --if-not-exists


if [[ -z $(php bin/console doctrine:migrations:diff -n --allow-empty-diff --quiet) ]]; then

    NEW_MIGRATION_FILE=$(ls -Art $MIGRATIONS_DIR/*.php | tail -n 1)
    
    if [[ -n "$NEW_MIGRATION_FILE" ]]; then
        NEW_MIGRATION_FILE_NAME=$(basename "$NEW_MIGRATION_FILE" .php)

        php bin/console doctrine:migrations:migrate "$MIGRATIONS_NAMESPACE\\$NEW_MIGRATION_FILE_NAME" --no-interaction

        echo "Neue Migration $NEW_MIGRATION_FILE_NAME wurde ausgeführt";
    else
        echo "Keine Migrationsdatei gefunden.";
    fi
else
    echo "Keine neuen Migrationen gefunden.";
fi

php bin/console app:fill-database

# PHP-FPM starten
exec php-fpm