#!/bin/bash

# Konfiguration
DOMAIN="quizapp.space.local"
DAYS=365
CERT_DIR="docker/certs"

# Verzeichnis erstellen
rm -rf $CERT_DIR
mkdir -p $CERT_DIR
mkdir -p "$CERT_DIR/webserver"
mkdir -p "$CERT_DIR/mysql"

# Webserver-Zertifikate erstellen
echo "Erstelle Webserver-Zertifikate..."
openssl genrsa -out "$CERT_DIR/webserver/privkey.pem" 2048
openssl req -new -key "$CERT_DIR/webserver/privkey.pem" -out "$CERT_DIR/webserver/server.csr" -subj "/CN=$DOMAIN"
openssl x509 -req -days "$DAYS" -in "$CERT_DIR/webserver/server.csr" -signkey "$CERT_DIR/webserver/privkey.pem" -out "$CERT_DIR/webserver/fullchain.pem"
rm "$CERT_DIR/webserver/server.csr" # Entferne die CSR-Datei


# MySQL-SSL/TLS-Zertifikate erstellen
echo "Erstelle MySQL-SSL/TLS-Zertifikate..."
openssl genrsa -out "$CERT_DIR/mysql/privkey.pem" 2048
openssl req -new -key "$CERT_DIR/mysql/privkey.pem" -out "$CERT_DIR/mysql/server.csr" -subj "/CN=$DOMAIN"
openssl x509 -req -days "$DAYS" -in "$CERT_DIR/mysql/server.csr" -signkey "$CERT_DIR/mysql/privkey.pem" -out "$CERT_DIR/mysql/server-cert.pem"
cp "$CERT_DIR/mysql/server-cert.pem" "$CERT_DIR/mysql/cert.pem" # Für MySQL cert.pem = server-cert.pem
rm "$CERT_DIR/mysql/server.csr" # Entferne die CSR-Datei

# Berechtigungen und Gruppe setzen
echo "Setze Berechtigungen und Gruppe..."
chmod -R 777 "$CERT_DIR"
chown -R root:root "$CERT_DIR"

echo "Zertifikate wurden in '$CERT_DIR' erstellt."