#!/bin/bash

DIR=""
CONFIG_DIR="./docker/config"
SECRET_DIR="./docker/secrets"

# Überprüfen des Arguments
if [ -z "$1" ]; then
  echo "Param 'secret' oder 'config' ist erforderlich."
  exit 1
fi

# Setzen von DIR basierend auf dem Argument
if [ "$1" = "secret" ]; then
  DIR="$SECRET_DIR"
elif [ "$1" = "config" ]; then
  DIR="$CONFIG_DIR"
else
  echo "Error: Ungültiges Argument '$1'."
  echo "Nur 'secret' oder 'config' sind gültige Parameter."
  exit 1
fi

for file in "$DIR"/*.txt "$DIR"/*.env; do

    if [ ! -e "$file" ]; then
        continue
    fi

    name=$(basename "$file")

    if [ "$1" = "config" ]; then
        name=$(echo "$name" | sed 's/\./-/g')
        echo "$name"
        docker "$1" rm "$name"
        docker "$1" create "$name" "$file"
        echo "Config wird erstellt: $name"
    else
        name_without_extension="${name%.*}"
        docker "$1" rm "$name_without_extension"
        docker "$1" create "$name_without_extension" "$file"
        echo "Config wird erstellt: $name_without_extension"
    fi
done