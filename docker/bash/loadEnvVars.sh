#!/bin/bash
set -e

CONFIG_DIR="/run/configs"
SECRET_DIR="/run/secrets"

load_env_vars_from_file() {
  local file="$1"
  local source_type="$2"

  if [ -n "$file" ] && [ -f "$file" ]; then
    while IFS='=' read -r key value; do
      if [[ "$key" != "" && "$value" != "" ]]; then
        export "$key=$value"
      fi
    done < "$file"
  else
    echo "Error: Source for $source_type is not valid: $file"
  fi
}

load_secret_vars() {
  local secret_dir="$1"

  if [ -d "$secret_dir" ]; then
    for secret_file in "$secret_dir"/*; do
      if [ -f "$secret_file" ]; then
        secret_name=$(basename "$secret_file" | sed 's/\..*$//' | tr '[:lower:]' '[:upper:]')
        secret_value=$(cat "$secret_file")

        export "$secret_name=$secret_value"
      fi
    done
  else
    echo "Error: Secrets directory not found: $secret_dir"
  fi
}


if [ -d "$CONFIG_DIR" ]; then
  for config_file in "$CONFIG_DIR"/*; do
    load_env_vars_from_file "$config_file" "config"
  done
else
  echo "Error: Config directory not found: $CONFIG_DIR"
fi

load_secret_vars "$SECRET_DIR"